<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'devmindz_riverstone2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HSjdw%4%zo(S|g @AG3AH$ZiKoj_n6Poq&n|m+I&oGf[*g2?HBbvfP!Xl@=&4u L');
define('SECURE_AUTH_KEY',  '5<9xvcMo&Cox{=BH~YlIWRhvt>3X/XkHKi+W+Pj8EtN|5Ft((=25)6=Od/:.U^sv');
define('LOGGED_IN_KEY',    'd>G^g*eGuK1_QzlUGccb76n}oEubdlA2Tdja;SWka5?TYf>4!U.kkp8ys1*AfO&y');
define('NONCE_KEY',        'S7V<vqYdp,;`$2T9b, 4SPw<7Xh<h s(.JS}$.Uv0O*sy 9Hi~{r|%3 {WbI;R^U');
define('AUTH_SALT',        '%o<A|$O1&UR>=$V7&3O3o-5w31B4_0,%$sp^vHU86_E>l aPZ6d?#Ie2vJE2/r7J');
define('SECURE_AUTH_SALT', '[gX{J!LQ]^bk3xJ>h9Vh<<H9/ZH6J)+@}D=,;3LKjXgnw!#J*!UpGto$`s`4^=*b');
define('LOGGED_IN_SALT',   'XU$A[<R+ 3R}+F@KG~|TfIq5<}OjSEP-yjSo%A@S/:cFwz.izcwy[a,yuToZTa(L');
define('NONCE_SALT',       'J;)Q/]5o#0_#$%$w7>(FgPkxLBs>KX6f?21YujZC/`dVTCz9Bmj3,ch6} Z#uv+)');

/**#@-*/
define('FS_METHOD', 'direct');
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
