<?php 
/**
 * The Sidebar for Playschool
 *
 * Stores the sidebar area of the template. loaded in other template files with get_sidebar();
 *
 * @package play-school
 * 
 * @since play-school 1.0
 */
global $play;?>
 
  <div id="sidebar">
  <div class="widgets">
    <?php if ( ! dynamic_sidebar( 'sidebar-page' ) ) : ?>
    <div class="widget">
      <div class="widget_wrap">
        <h3 class="widget-title">
          <?php _e( 'Pages', 'play-school' ); ?>
        </h3>
        <span class="widget_border"></span>
        <ul>
          <?php wp_list_pages( array( 'title_li' => '' ) ); ?>
        </ul>
      </div>
    </div>
    <?php endif; // end sidebar widget area ?>
  </div>
</div>
 
 
