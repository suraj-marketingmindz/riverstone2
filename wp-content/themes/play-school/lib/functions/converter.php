<?php
add_action( 'init', 'complete_convert_redux' );
function complete_convert_redux() {

	
if(isset($_POST['convert']) && check_admin_referer( 'complete_convert', 'complete_convert' ) ) {
	$play = get_option('complete');
	$active_widgets = get_option( 'sidebars_widgets' );
	$home_blocks = $play['home_sort_id'];

if ($home_blocks):

foreach ($home_blocks as $key=>$value) {

    switch($key) {
	//About Section
    case 'about':
		if(!empty($home_blocks['about'])){
				//ABOUT SECTION--------------------------------------------
				$active_widgets[ 'front_sidebar' ][] = 'complete_front_about-1';
				// The latest 15 questions from WordPress Stack Exchange.
				$about_content[ 1 ] = array (
					'title' => $play['about_header_id'],
					'subtitle' => $play['about_preheader_id'],
					'content' => $play['about_content_id'],
					'divider' => $play['divider_icon'],
					'title_color' => $play['about_header_color'],
					'content_color' => $play['about_text_color'],
					'content_bg' => $play['about_bg_color'],
				);
				update_option( 'widget_complete_front_about', $about_content );
		}
	
	break;
    case 'blocks':
		if(!empty($home_blocks['blocks'])){
				//BLOCKS SECTION--------------------------------------------
				$active_widgets[ 'front_sidebar' ][] = 'complete_front_blocks-1';
				// The latest 15 questions from WordPress Stack Exchange.
				$blocks_content[ 1 ] = array (
					'block1title' => $play['block1_text_id'],
					'block1img' => $play['block1_image']['url'],
					'block1content' =>  $play['block1_textarea_id'],
					'block2title' => $play['block2_text_id'],
					'block2img' => $play['block2_image']['url'],
					'block2content' =>  $play['block2_textarea_id'],
					'block3title' => $play['block3_text_id'],
					'block3img' => $play['block3_image']['url'],
					'block3content' => $play['block3_textarea_id'],
					'block4title' => $play['block4_text_id'],
					'block4img' => $play['block4_image']['url'],
					'block4content' => $play['block4_textarea_id'],
					'block5title' => $play['block5_text_id'],
					'block5img' => $play['block5_image']['url'],
					'block5content' => $play['block5_textarea_id'],
					'block6title' => $play['block6_text_id'],
					'block6img' => $play['block6_image']['url'],
					'block6content' => $play['block6_textarea_id'],
					
					'blockstitlecolor' => $play['blocktitle_color_id'],
					'blockstxtcolor' => $play['blocktxt_color_id'],
					'blocksbgcolor' => $play['midrow_color_id'],
				);
				update_option( 'widget_complete_front_blocks', $blocks_content );
				
		}
	break;
	
	
	case 'welcome-text':
		if(!empty($home_blocks['welcome-text'])){
				//WELCOME TEXT SECTION--------------------------------------------
				$active_widgets[ 'front_sidebar' ][] = 'complete_front_text-1';
				// The latest 15 questions from WordPress Stack Exchange.
				$text_content[ 1 ] = array (
					'title' => __('Welcome Text','play-school'),
					'content' => $play['welcm_textarea_id'],
					'padtopbottom' => '2',
					'paddingside' => '2',
					'parallax' => '',
					'content_color' => $play['welcometxt_color_id'],
					'content_bg' => $play['welcome_color_id'],
					'content_bgimg' => $play['welcome_bg_image']['url'],
				);
				//Updated Below --With Newsletter Widget
				if(empty($home_blocks['newsletter'])){
					update_option( 'widget_complete_front_text', $text_content );	
				}
				
		}
	break;
	
	
	case 'posts':
		if(!empty($home_blocks['posts'])){
				//POSTS SECTION--------------------------------------------
				$active_widgets[ 'front_sidebar' ][] = 'complete_front_posts-1';
				// The latest 15 questions from WordPress Stack Exchange.
				$posts_content[ 1 ] = array (
					'title' => $play['posts_title_id'],
					'subtitle' => $play['posts_subtitle_id'],
					'layout' => $play['front_layout_id'],
					'type' => $play['n_posts_type_id'],
					'count' => $play['n_posts_field_id'],
					'category' => $play['posts_cat_id'],
					'divider' => $play['divider_icon'],
					'navigation' => $play['navigation_type'],
					'postbgcolor' => $play['frontposts_color_id'],
					'titlecolor' => $play['frontposts_title_color'],
					'secbgcolor' => $play['frontposts_bg_color'],
				);
				update_option( 'widget_complete_front_posts', $posts_content );
		}
	break;
	
	
	
    } //end of SWITCH

} //end of FOREACH

endif;
		
		//Assign Widgets to frontpage sidebar
		update_option( 'sidebars_widgets', $active_widgets );
		//Update convert =1  and Nivo/Accordion Slides
		$play['converted'] = '1';
		update_option( 'complete', $play );
	
		//After Conversion Redirect to Customizer
        $redirect = admin_url('/customize.php'); 
		wp_redirect( $redirect);
	}
}