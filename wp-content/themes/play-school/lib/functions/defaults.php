<?php
function complete_option_defaults() {
	$defaults = array(
		'converted' => '',
		'site_layout_id' => 'site_full',
		'single_post_layout_id' => 'single_layout1',
		'header_layout_id' => 'head-type1',
		'center_width' => 83.50,
		'content_bg_color' => '#ffffff',
		'divider_icon' => 'fa-stop',
		'head_transparent' => '',
		'trans_header_color' => '#fff',
		'totop_id' => '1',
		'footer_text_id' => __('Copyright 2016 SKT Themes | All Rights Reserved | Powered by <a href="http://www.sktthemes.net/" target="_blank" rel="nofollow">sktthemes.net</a>', 'play-school'),
		'phntp_text_id' => __('<i class="fa fa-phone"></i> +12 345 67890', 'play-school'),
		'emltp_text' => __('<a href="mailto:info@sitename.com"><i class="fa fa-envelope"></i>info@sitename.com </a>', 'play-school'),
		'sintp_text' => __('[social_area]
    [social icon="facebook" link="#"]
    [social icon="twitter" link="#"]
    [social icon="google-plus" link="#"]	
    [social icon="linkedin" link="#"]
[/social_area]', 'play-school'),
		'suptp_text' => __('[searchform]', 'play-school'),
		'footmenu_id' => '1',
		'copyright_center' => '',
		
		'custom_slider' => '',
		
		'slider_type_id' => 'static',
		'n_slide_time_id' => '6000',
		'slide_height' => '500px',
		'slidefont_size_id' => '36px',
		'slider_txt_hide' => '',

		'post_info_id' => '1',
		'post_nextprev_id' => '1',
		'post_comments_id' => '1',
		'page_header_color' => '#b5503d',
		'pageheader_bg_image' => '',
		'hide_pageheader' => '',
		'page_header_txtcolor' => '#555555',
		
		'post_header_color' => '#b5503d',
		'postheader_bg_image' => '',
		'hide_postheader' => '',		

		'blog_cat_id' => '',
		'blog_num_id' => '9',
		'blog_layout_id' => '',
		'show_blog_thumb' => '1',
		
		'sec_color_id' => '#fec42b',
		'submnu_textcolor_id' => '#000000',
		'submnbg_color_id' => '#ffffff',
		'mnshvr_color_id' => '#ffffff',
		'mobbg_color_id' => '#383939',
		'mobbgtop_color_id' => '#fec42b',
		'mobmenutxt_color_id' => '#FFFFFF',
		
		'mobtoggle_color_id' => '#000000',
		'mobtoggleinner_color_id' => '#FFFFFF',
		
		'sectxt_color_id' => '#FFFFFF',
		'content_font_id' =>  array('font-family' => 'Lato', 'font-size' => '14px'),
		'primtxt_color_id' => '#727370',
		'logo_image_id' => array(  'url'=>''.get_template_directory_uri().'/assets/images/logo.png'),
		'logo_font_id' => array('font-family' => 'Open Sans', 'font-size' => '36px'),
		'logo_color_id' => '#555555',
		
		'logo_image_height' => '78px;',
		'logo_image_width' => '254px;',
		'logo_margin_top' => '18px;',
		
		'tpbt_font_id' => array('font-family' => 'Arimo', 'font-size' => '14px'),
		'tpbt_color_id' => '#949494',
		'tpbt_hvcolor_id' => '#fec42b',
		
		'topbordercolor' => '#fec42b',
		
		'topinnerborder' => '#ebebeb',
		'toplefticoncolor' => '#fec42b',	
		
		'sldtitle_font_id' => array('font-family' => 'Montserrat', 'font-size' => '36px'),
		'slddesc_font_id' => array('font-family' => 'Roboto', 'font-size' => '14px'),
		'sldbtn_font_id' => array('font-family' => 'Lato', 'font-size' => '14px'),
		
		'slidetitle_color_id' => '#4b4c47',	
		'slddesc_color_id' => '#696969',	
		'sldbtntext_color_id' => '#ffffff',
		'sldbtn_color_id' => '#669d89',
		'sldbtn_hvcolor_id' => '#ea5330',	
		
		'slide_pager_color_id' => '#fec42b',	
		'slide_active_pager_color_id' => '#ea5330',	
		
			
		'global_link_color_id' => '#fec42b',
		'global_link_hvcolor_id' => '#999999',		
		
		'global_h1_color_id' => '#4b4c47',
		'global_h1_hvcolor_id' => '#fec42b',	
		'global_h2_color_id' => '#4b4c47',
		'global_h2_hvcolor_id' => '#fec42b',
		'global_h3_color_id' => '#4b4c47',
		'global_h3_hvcolor_id' => '#fec42b',
		'global_h4_color_id' => '#4b4c47',
		'global_h4_hvcolor_id' => '#fec42b',
		'global_h5_color_id' => '#4b4c47',
		'global_h5_hvcolor_id' => '#fec42b',
		'global_h6_color_id' => '#4b4c47',
		'global_h6_hvcolor_id' => '#fec42b',	
		
		'post_meta_color_id' => '#000000',
		'team_box_color_id' => '#f7f7f7',
		
		'social_text_color_id' => '#bab9b9',
		'social_border_color' => '#ebebeb',
		'social_icon_color_id' => '#fdfcf7',
		'social_hover_icon_color_id' => '#fec42b',
		'testimonialbox_color_id' => '#f7f7f7',		
		'testimonial_pager_color_id' => '#ffffff',
		'testimonial_activepager_color_id' => '#000000',
		'testimonial_rotator_color' => '#ffffff',
		'gallery_filter_color_id' => '#fec42b',
		'gallery_filtertxt_color_id' => '#000000',
		'gallery_activefiltertxt_color_id' => '#ffffff',
		'skillsbar_bgcolor_id' => '#464646',
		'skillsbar_text_color_id' => '#ffffff',								
		'global_h1_font_id' => array('font-family' => 'Montserrat', 'font-size' => '30px'),
		'global_h2_font_id' => array('font-family' => 'Montserrat', 'font-size' => '25px'),
		'global_h3_font_id' => array('font-family' => 'Montserrat', 'font-size' => '20px'),
		'global_h4_font_id' => array('font-family' => 'Montserrat', 'font-size' => '13px'),
		'global_h5_font_id' => array('font-family' => 'Montserrat', 'font-size' => '11px'),
		'global_h6_font_id' => array('font-family' => 'Montserrat', 'font-size' => '9px'),
		
		'contact_title' => 'Contact Info',
		'contact_address' => 'Donec ultricies mattis nulla Australia',
		'contact_phone' => '0789 256 321',
		'contact_email' => 'info@companyname.com',
		'contact_company_url' => 'http://demo.com',
		'contact_google_map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d336003.6066860609!2d2.349634820486094!3d48.8576730786213!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x040b82c3688c9460!2sParis%2C+France!5e0!3m2!1sen!2sin!4v1433482358672',
		
		'head_bg_trans' => '1',
		'head_color_id' => '#f7f7f7',
		'head_info_color_id' => '#fdfcf7',
		'header_border_color' => '#dddddd',
		'menutxt_color_id' => '#818181',
		'menutxt_color_hover' => '#ea5330',
		'menutxt_color_active' => '#ea5330',
		'menu_size_id' => '13px',
		'sidebar_color_id' => '#FFFFFF',
		'sidebarborder_color_id' => '#eeeff5',
		'sidebar_tt_color_id' => '#666666',
		'sidebartxt_color_id' => '#999999',
		'sidebarlink_color_id' => '#b5503d',
		'sidebarlink_hover_color_id' => '#999999',
		'flipbg_front_color_id' => '#ffffff',
		'flipbg_back_color_id' => '#f7f7f7',
		'flipborder_front_color_id' => '#e0e0e0',
		'flipborder_back_color_id' => '#000000',
		'divider_color_id' => '#8c8b8b',
		'wgttitle_size_id' => '16px',
		'timebox_color_id' => '#ffffff',
		'timeboxborder_color_id' => '#dedede',
		'gridbox_color_id' => '#ffffff',
		'gridboxborder_color_id' => '#cccccc',
		'courseblockbg1' => '#a1d042',
		'courseblockbg2' => '#ffba41',
		'courseblockbg3' => '#f13d6d',
		'courseblockbg4' => '#1c3b9f',
		
		'courseblockcolor' => '#ffffff',
		'courseblockhovercolor' => '#000000',
		'workinghrscolors' => '#ffffff',
		'workingbgheighlights' => '#f13e3e',
		'historyborder' => '#ff9c00',
		
		'tabstextcolor' => '#ffffff',
		'tabscolor' => '#707070',
		'activetabscolor' => '#ea5330',
		
		'foot_layout_id' => '4',
		'footer_color_id' => '#414b4f',
		'footwdgtxt_color_id' => '#cccccc',
		'footer_title_color' => '#ffffff',
		
		'footer_link_color' => '#cccccc',
		'footer_link_hover_color' => '#ffffff',
		
		'ptitle_font_id' =>  array('font-family' => 'Lato', 'subsets'=>'latin'),
		'mnutitle_font_id' =>  array('font-family' => 'Montserrat', 'subsets'=>'latin'),
		'title_txt_color_id' => '#666666',
		'link_color_id' => '#3590ea',
		'link_color_hover' => '#1e73be',
		'txt_upcase_id' => '1',
		'mnutxt_upcase_id' => '1',
		'copyright_bg_color' => '#414b4f',
		'copyrightbordercolor' => '#5e676b',
		'copyright_txt_color' => '#ffffff',
		
		//Featured Box
		'featured_section_title' => __('', 'play-school'),
		'homeblock_bg_setting' => '',
		'ftd_bg_video' => '',
		'homeblock_title_color' => '#000000',
		'homeblock_color_id' => '#fdfcf7',
		'featured_image_height' => '96px;',
		'featured_image_width' => '87px;',
		'featured_excerpt' => '35',
		'featured_block_bg' => '#ffffff',
		'featured_block_button' => __('Read More', 'play-school'),
		'recentpost_block_button' => __('Read More', 'play-school'),
		'courses_block_button' => __('Apply Now', 'play-school'),
		'teacher_block_button' => __('Send Email', 'play-school'),

		'featured_block_button_bg' => '#669d89',
		'featured_block_hover_button_bg' => '#ea5330',
		
		'page-setting1_image' => array(  'url'=>''.get_template_directory_uri().'/images/featured_icon1.png'),
		'page-setting2_image' => array(  'url'=>''.get_template_directory_uri().'/images/featured_icon2.png'),
		'page-setting3_image' => array(  'url'=>''.get_template_directory_uri().'/images/featured_icon3.png'),
		
		'page-setting1' => '0',
		'page-setting2' => '0',
		'page-setting3' => '0',
		'page-setting4' => '0',
		'hide_boxes' => '',
		
		//Home Section1
		'section1_title' => __('', 'play-school'),
		'section1_bgcolor_id' => '#ffffff',
		'section1_bg_image' => '',
		'section1_bg_video' => '',
		'section1_content' => '[su_row] [su_column size="2/5"] <img src="'.get_template_directory_uri().'/images/friendly.png" />[/su_column] [su_column size="3/5"] <h2>Friendly Atmosphere</h2> <div>Maecenas ullamcorper nec ipsum in lacinia</div>
[su_spacer size="20"]

[infolist 
thumb="'.get_template_directory_uri().'/images/facility-icon.png" 
title="playground facility" 
content="Sed scelerisque tincidunt est venenatis laoreet. Nam eget enim velit. Curabitur nisi massa, sagittis sit amet libero in, lacinia facilisis dolor. Sed et enim vel dolor tempus viverra. Suspendisse elit lorem, vehicula vel tempor eget"]

[infolist
thumb="'.get_template_directory_uri().'/images/expert-teachers-icon.png"
title="Expert Teachers"
content="Sed scelerisque tincidunt est venenatis laoreet. Nam eget enim velit. Curabitur nisi massa, sagittis sit amet libero in, lacinia facilisis dolor. Sed et enim vel dolor tempus viverra. Suspendisse elit lorem, vehicula vel tempor eget"]

[infolist
thumb="'.get_template_directory_uri().'/images/sessions-icon.png"
title="Full Day Sessions" 
content="Sed scelerisque tincidunt est venenatis laoreet. Nam eget enim velit. Curabitur nisi massa, sagittis sit amet libero in, lacinia facilisis dolor. Sed et enim vel dolor tempus viverra. Suspendisse elit lorem, vehicula vel tempor eget"]
		[/su_column] [/su_row]',
		'hide_boxes_section1' => '',
		//Home Section1
		
		//Home Section2
		'section2_title' => __('Our School Gallery', 'play-school'),
		'section2_bgcolor_id' => '#fdfcf7',
		'section2_bg_image' => '',
		'section2_bg_video' => '',
		'section2_content' => '[photogallery filter="false"]',
		'hide_boxes_section2' => '',
		//Home Section2	
		
		//Home Section3
		'section3_title' => __('Featured Courses', 'play-school'),
		'section3_bgcolor_id' => '#ffffff',
		'section3_bg_image' => '',
		'section3_bg_video' => '',
		'section3_content' => '[courses-box col="3" show="3" catslug="" excerptlength="30"]',
		'hide_boxes_section3' => '',
		//Home Section3	
		
		//Home Section4
		'section4_title' => __('Our Teachers', 'play-school'),
		'section4_bgcolor_id' => '#fdfcf7',
		'section4_bg_image' => '',
		'section4_bg_video' => '',
		'section4_content' => '[ourteachers col="2" show="4" excerptlength="25"]',
		'hide_boxes_section4' => '',
		//Home Section4		
		
		//Home Section5
		'section5_title' => __('Tabs', 'play-school'),
		'section5_bgcolor_id' => '#ffffff',
		'section5_bg_image' => '',
		'section5_bg_video' => '',
		'section5_content' => '[su_row]
[su_column size="1/2"]
<h3>Default tab</h3>
[su_tabs]
[su_tab title="Tab 1"] Tab 1 content [/su_tab]
[su_tab title="Tab 2"] Tab 2 content [/su_tab]
[su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs][/su_column]
[su_column size="1/2"]
<h3>Custom active tab</h3>
[su_tabs active="2"]
[su_tab title="Tab 1"] Tab 1 content [/su_tab]
[su_tab title="Tab 2"] Tab 2 content [/su_tab]
[su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs][/su_column]
[/su_row]

[su_row]
[su_column size="1/2"]
<h3>Vertical tabs</h3>
[su_tabs vertical="yes"]
  [su_tab title="Tab 1"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs]
[/su_column]

[su_column size="1/2"]
<h3>Tabs anchors</h3>
[su_tabs]
  [su_tab title="Tab 1" anchor="First"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2" anchor="Second"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3" anchor="Third"] Tab 3 content [/su_tab]
[/su_tabs]

Use next links to switch this tabs

<a href="#First">Open tab 1</a> | <a href="#Second">Open tab 2</a> | <a href="#Third">Open tab 3</a>
[/su_column]
[/su_row]

[su_row]
[su_column size="1/2"]
<h3>Disabled tabs</h3>
[su_tabs]
  [su_tab title="Tab 1"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3"] Tab 3 content [/su_tab]
  [su_tab title="Tab 4 (disabled)" disabled="yes"] Tab 4 content [/su_tab]
  [su_tab title="Tab 5 (disabled)" disabled="yes"] Tab 5 content [/su_tab]
[/su_tabs]
[/su_column]
[su_column size="1/2"]
<h3>Custom styles</h3>
[su_tabs class="my-custom-tabs"]
  [su_tab title="Tab 1"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs]
[/su_column]
[/su_row]',
		'hide_boxes_section5' => '1',
		//Home Section5		
		
		//Home Section6
		'section6_title' => __('', 'play-school'),
		'section6_bgcolor_id' => '',
		'section6_bg_image' => ''.get_template_directory_uri().'/images/testimonial-section-bg.jpg',
		'section5_bg_video' => '',
		'section6_content' => '[testimonials-rotator show="3"]',
		'hide_boxes_section6' => '',
		//Home Section6	
		
		//Home Section7
		'section7_title' => __('Latest Blog Posts', 'play-school'),
		'section7_bgcolor_id' => '#fdfcf7',
		'section7_bg_image' => '',
		'section7_bg_video' => '',
		'section7_content' => '[posts-style3 show="3" cat="1" excerptlength="24"]',
		'hide_boxes_section7' => '',
		//Home Section7		
		
		//Home Section8
		'section8_title' => __('Features', 'play-school'),
		'section8_bgcolor_id' => '#f2f2f2',
		'section8_bg_image' => '',
		'section8_bg_video' => '',
		'section8_content' => '[posts-timeline show="12" cat="1" excerptlength="28"]',
		'hide_boxes_section8' => '1',
		//Home Section8		
		
		//Home Section9
		'section9_title' => __('Toggle & Skills', 'play-school'),
		'section9_bgcolor_id' => '',
		'section9_bg_image' => ''.get_template_directory_uri().'/images/skills-bg.jpg',
		'section9_bg_video' => '',
		'section9_content' => '[su_row]
  [su_column size="1/2"]
  [su_accordion]
  [su_spoiler title="Vision and Mission" open="yes"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique fermentum ullamcorper. Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet.[/su_spoiler]
  [su_spoiler title="Company Philopsphy"]Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique fermentum ullamcorper. [/su_spoiler]
  [su_spoiler title="Spoiler title"]Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique fermentum ullamcorper. Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit.[/su_spoiler]
[/su_accordion]
  [/su_column]
  [su_column size="1/2"] [skill title="HTML" percent="80" bgcolor="#ff7800"][skill title="Wordpress" percent="99" bgcolor="#ff7800"][skill title="Web Design" percent="90" bgcolor="#ff7800"][skill title="Web Development" percent="95" bgcolor="#ff7800"][skill title="Responsive" percent="85" bgcolor="#ff7800"] [/su_column]
[/su_row]',
		'hide_boxes_section9' => '1',
		//Home Section9	
		
		//Home Section10
		'section10_title' => __('List Type', 'play-school'),
		'section10_bgcolor_id' => '#ffffff',
		'section10_bg_image' => '',
		'section10_bg_video' => '',
		'section10_content' => '[su_row]
  [su_column size="1/3"] [su_list icon="icon: arrow-circle-right" icon_color="#fec42b"]
<ul>
	<li>Lorem ipsum dolor sit amet</li>
	<li>Habitant morbi tristique senectus et</li>
	<li>Suspendisse ut interdum risus</li>
	<li>Pellentesque mi at blandit</li>
</ul>
[/su_list]

[su_list icon="icon: check" icon_color="#ce7b4e"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list] 

[/su_column]
  [su_column size="1/3"] [su_list icon="icon: long-arrow-right" icon_color="#fec42b"]<ul>
	<li>Lorem ipsum dolor sit amet</li>
	<li>Habitant morbi tristique senectus et</li>
	<li>Suspendisse ut interdum risus</li>
	<li>Pellentesque mi at blandit</li>
</ul>[/su_list]

[su_list icon="icon: arrow-circle-right" icon_color="#fec42b"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list]

[/su_column]

[su_column size="1/3"]
	[su_list icon="icon: hand-o-right" icon_color="#e15453"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list]

 [su_list icon="icon: plane" icon_color="#53e1d9"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list]
	
[/su_column]
[/su_row]',
		'hide_boxes_section10' => '1',
		//Home Section10
		
		//Home Section11
		'section11_title' => __('Our Partners', 'play-school'),
		'section11_bgcolor_id' => '#ffffff',
		'section11_bg_image' => '',
		'section11_bg_video' => '',
		'section11_content' => '[client url="#" image="'. get_template_directory_uri().'/images/wp.png"]<br /> [client url="#" image="'. get_template_directory_uri().'/images/woocommerce.jpg"]<br /> [client url="#" image="'. get_template_directory_uri().'/images/bbpress.jpg"]<br /> [client url="#" image="'. get_template_directory_uri().'/images/jquery.jpg"]<br /> [client url="#" image="'. get_template_directory_uri().'/images/mysql.jpg"]<br /> [client url="#" image="'. get_template_directory_uri().'/images/bbpress-1.jpg"]',
		'hide_boxes_section11' => '1',
		//Home Section11	
		
		
		//Footer Column 1
		'foot_cols1_title' => __('About Us', 'play-school'),
		'foot_cols1_content' => '<p>Sed suscipit mauris nec mauris vulputate, a posuere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula</p><p>Nam metus lorem, hendrerit quis ante eget, lobortis elementum neque. Aliquam in ullamcorper quam. Integer euismod ligula in mauris vehicula imperdiet. Cras in convallis ipsum. Phasellus tortor turpis.</p>',
		//Footer Column 1	
		
		//Footer Column 2
		'foot_cols2_title' => __('Quick Links', 'play-school'),
		'foot_cols2_content' => '[footermenu]',
		//Footer Column 2	
		
		//Footer Column 3
		'foot_cols3_title' => __('Contact Us', 'play-school'),
		'foot_cols3_content' => '<p><i class="fa fa-map-marker" aria-hidden="true"></i>
 Best Avenue 16-2, CA 23653, USA</p><p><i class="fa fa-phone" aria-hidden="true"></i>
 +1 998 71 150 30 20</p><p><i class="fa fa-fax" aria-hidden="true"></i>
 +1 998 71 150 30 20</p><p><i class="fa fa-envelope-o" aria-hidden="true"></i>
 <a href="mailto:info@sitename.com">info@sitename.com</a></p>',
		//Footer Column 3
		
		//Footer Column 4
		'foot_cols4_title' => __('Working Hours', 'play-school'),
		'foot_cols4_content' => '[timing day="Monday" hours="10 AM to 7pm"][timing day="Tuesday" hours="10 AM to 7pm"][timing day="Wednesday" hours="10 AM to 7pm"][timing day="Thursday" hours="10 AM to 7pm"][timing day="Friday" hours="10 AM to 7pm"][timing day="Saturday" hours="Closed" highlight="yes"][timing day="Sunday" hours="Closed" highlight="yes"]',
		//Footer Column 4																
		'social_button_style' => 'simple',
		'social_show_color' => '',
		'social_bookmark_pos' => 'footer',
		'social_bookmark_size' => 'normal',
		'social_single_id' => '1',
		'social_page_id' => '',
		
		'post_lightbox_id' => '1',
		'post_gallery_id' => '1',
		'cat_layout_id' => '4',
		'hide_mob_slide' => '',
		'hide_mob_rightsdbr' => '',
		'hide_mob_page_header' => '1',
		'custom-css' => '',
	);
	
      $options = get_option('complete',$defaults);

      //Parse defaults again - see comments
      $options = wp_parse_args( $options, $defaults );

	return $options;
}?>