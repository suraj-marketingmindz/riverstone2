<?php 
/**
 * The SHARE THIS Function for Playschool
 *
 * Displays The social Bookmark icons on single posts page.
 *
 * @package complete
 * 
 * @since  complete 1.0
 */
global $play;?>

<div class="social_bookmarks<?php if(!empty($play['social_show_color'])) { ?> social_color<?php } ?> bookmark_<?php echo $play['social_button_style'];?> bookmark_size_<?php echo $play['social_bookmark_size']; ?>">
	  <?php if(!empty($play['facebook_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_fb" href="<?php echo esc_url($play['facebook_field_id']); ?>"><i class="fa-facebook"></i></a>
      <?php } ?>
      <?php if(!empty($play['twitter_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_twt" href="<?php echo esc_url($play['twitter_field_id']); ?>"><i class="fa-twitter"></i></a><?php } ?>
      <?php if(!empty($play['gplus_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_gplus" href="<?php echo esc_url($play['gplus_field_id']); ?>"><i class="fa-google-plus"></i></a> 
      <?php } ?>
      <?php if(!empty($play['youtube_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_ytb" href="<?php echo esc_url($play['youtube_field_id']); ?>"><i class="fa-youtube-play"></i></a>
      <?php } ?>
      <?php if(!empty($play['flickr_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_flckr" href="<?php echo esc_url($play['flickr_field_id']); ?>"><i class="fa-flickr"></i></a>
      <?php } ?>
      <?php if(!empty($play['linkedin_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_lnkdin" href="<?php echo esc_url($play['linkedin_field_id']); ?>"><i class="fa-linkedin"></i></a>
      <?php } ?>
      <?php if(!empty($play['pinterest_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_pin" href="<?php echo esc_url($play['pinterest_field_id']); ?>"><i class="fa-pinterest"></i></a>
      <?php } ?>
      <?php if(!empty($play['tumblr_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_tmblr" href="<?php echo esc_url($play['tumblr_field_id']); ?>"><i class="fa-tumblr"></i></a>
      <?php } ?>
      <?php if(!empty($play['dribble_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_dribble" href="<?php echo esc_url($play['dribble_field_id']); ?>"><i class="fa-dribbble"></i></a>
      <?php } ?>
      <?php if(!empty($play['behance_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_behance" href="<?php echo esc_url($play['behance_field_id']); ?>"><i class="fa-behance"></i></a>
      <?php } ?>
      <?php if(!empty($play['instagram_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_insta" href="<?php echo esc_url($play['instagram_field_id']); ?>"><i class="fa-instagram"></i></a>
      <?php } ?>  
      <?php if(!empty($play['rss_field_id']) || is_customize_preview()){ ?>
      	<a target="_blank" class="ast_rss" href="<?php echo esc_url($play['rss_field_id']); ?>"><i class="fa-rss"></i></a>
      <?php } ?>   
</div>