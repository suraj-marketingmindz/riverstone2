<?php 
/**
 * The Sidebar for Playschool
 *
 * Stores the sidebar area of the template. loaded in other template files with get_sidebar();
 *
 * @package play-school
 * 
 * @since play-school 1.0
 */
global $play;?>

            <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
                <div id="sidebar" class="home_sidebar <?php if(!empty($play['hide_mob_rightsdbr'])){ echo 'hide_mob_rightsdbr';} ?>">
                    <div class="widgets">  
                            <?php dynamic_sidebar('sidebar'); ?>
                     </div>
                 </div>
            <?php endif; ?>
