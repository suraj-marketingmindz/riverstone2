<?php

//----------------------HOME FOUR BLOCKS FROM PAGES-------------------------------------

	$wp_customize->add_setting('complete[featured_section_title]', array(
		'type' => 'option',
		'default'	=> __('Featured Boxes','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'featured_section_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_blocks',
		'settings' => 'complete[featured_section_title]',
	)) );

	$wp_customize->add_setting('page-setting1',array(
			'sanitize_callback'	=> 'complete_sanitize_integer',
			'default' => '0',
			'capability' => 'edit_theme_options',				
	));
	
	$wp_customize->add_control('page-setting1',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box one:','play-school'),
			'section'	=> 'home_blocks'	
	));
	
//  Page 1 Image
	$wp_customize->add_setting( 'complete[page-setting1_image][url]',array( 
		'type' => 'option',
		'default' => ''. get_template_directory_uri().'/images/featured_icon1.png',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'page-setting1_image',array(
			'label'       => __( 'Select image for page box one', 'play-school' ),
			'section'     => 'home_blocks',
			'settings'    => 'complete[page-setting1_image][url]'
				)
			)
	);	
	
	$wp_customize->add_setting('page-setting2',array(
			'sanitize_callback'	=> 'complete_sanitize_integer',
			'default' => '0',
			'capability' => 'edit_theme_options',
	));
	
	$wp_customize->add_control('page-setting2',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box two:','play-school'),
			'section'	=> 'home_blocks'
	));
	
//  Page 2 Image
	$wp_customize->add_setting( 'complete[page-setting2_image][url]',array( 
		'type' => 'option',
		'default' => ''. get_template_directory_uri().'/images/featured_icon2.png',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'page-setting2_image',array(
			'label'       => __( 'Select image for page box two', 'play-school' ),
			'section'     => 'home_blocks',
			'settings'    => 'complete[page-setting2_image][url]'
				)
			)
	);	
	
	$wp_customize->add_setting('page-setting3',array(
			'sanitize_callback'	=> 'complete_sanitize_integer',
			'default' => '0',
			'capability' => 'edit_theme_options',
	));
	
	$wp_customize->add_control('page-setting3',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box three:','play-school'),
			'section'	=> 'home_blocks'
	));	
	
//  Page 3 Image
	$wp_customize->add_setting( 'complete[page-setting3_image][url]',array( 
		'type' => 'option',
		'default' => ''. get_template_directory_uri().'/images/featured_icon3.png',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'page-setting3_image',array(
			'label'       => __( 'Select image for page box three', 'play-school' ),
			'section'     => 'home_blocks',
			'settings'    => 'complete[page-setting3_image][url]'
				)
			)
	);	
	
	$wp_customize->add_setting('page-setting4',array(
			'sanitize_callback'	=> 'complete_sanitize_integer',
			'default' => '0',
			'capability' => 'edit_theme_options',
	));
	
	$wp_customize->add_control('page-setting4',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box four:','play-school'),
			'section'	=> 'home_blocks'
	));	
	
//  Page 4 Image
	$wp_customize->add_setting( 'complete[page-setting4_image][url]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'page-setting4_image',array(
			'label'       => __( 'Select image for page box four', 'play-school' ),
			'section'     => 'home_blocks',
			'settings'    => 'complete[page-setting4_image][url]'
				)
			)
	);	
	
	
// Color Section Of Home Blocks

// Block Section Background Color
$wp_customize->add_setting( 'complete[homeblock_color_id]', array(
	'type' => 'option',
	'default' => '#fdfcf7',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'homeblock_color_id', array(
				'label' => __('Section Background Color','play-school'),
				'section' => 'home_blocks',
				'settings' => 'complete[homeblock_color_id]',
			) ) );
			
// Block Section Background Color
$wp_customize->add_setting( 'complete[featured_block_bg]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'featured_block_bg', array(
				'label' => __('Featured Block Background Color','play-school'),
				'section' => 'home_blocks',
				'settings' => 'complete[featured_block_bg]',
			) ) );		

// Block Section Background Image
$wp_customize->add_setting( 'complete[homeblock_bg_setting]',array( 
	'type' => 'option',
	'default' => '',
	'sanitize_callback' => 'esc_url_raw',
	)
);
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'homeblock_bg_setting',array(
					'label'       => __( 'Section Background Image', 'play-school' ),
					'section'     => 'home_blocks',
					'settings'    => 'complete[homeblock_bg_setting]'
						)
					)
			);
			
			
// Block Section Background Video
	$wp_customize->add_setting( 'complete[ftd_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'ftd_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_blocks',
			'settings'    => 'complete[ftd_bg_video]'
				)
			)
	);			

// Featured Block Image Height
$wp_customize->add_setting('complete[featured_image_height]', array(
	'type' => 'option',
	'default' => '96px',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('featured_image_height', array(
				'type' => 'text',
				'label' => __('Featured Block Image Height','play-school'),
				'section' => 'home_blocks',
				'settings' => 'complete[featured_image_height]',
							'input_attrs'	=> array(
								'class'	=> 'mini_control',
							)
			) );
			
// Featured Block Image Width
$wp_customize->add_setting('complete[featured_image_width]', array(
	'type' => 'option',
	'default' => '87px',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('featured_image_width', array(
				'type' => 'text',
				'label' => __('Featured Block Image Width','play-school'),
				'section' => 'home_blocks',
				'settings' => 'complete[featured_image_width]',
							'input_attrs'	=> array(
								'class'	=> 'mini_control',
							)
			) );
			
////
// Featured Block Excerpt Length
$wp_customize->add_setting('complete[featured_excerpt]', array(
	'type' => 'option',
	'default' => '35',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('featured_excerpt', array(
				'type' => 'text',
				'label' => __('Featured Block Excerpt Length','play-school'),
				'section' => 'home_blocks',
				'settings' => 'complete[featured_excerpt]',
							'input_attrs'	=> array(
								'class'	=> 'mini_control',
							)
			) );
////					
	 
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_blocks',
		'settings' => 'complete[hide_boxes]',
	)) );	 						

//----------------------HOME FOUR BLOCKS FROM PAGES-------------------------------------

//----------------------HOME SECTION 1----------------------------------

	$wp_customize->add_setting('complete[section1_title]', array(
		'type' => 'option',
		'default'	=> __('','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section1_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections1',
		'settings' => 'complete[section1_title]',
	)) );	
	
$wp_customize->add_setting('complete[section1_content]', array(
	'type' => 'option',
	'default' => '[su_row] [su_column size="2/5"] <img src="'.get_template_directory_uri().'/images/friendly.png" />[/su_column] [su_column size="3/5"] <h2>Friendly Atmosphere</h2> <div>Maecenas ullamcorper nec ipsum in lacinia</div>
[su_spacer size="20"]

[infolist 
thumb="'.get_template_directory_uri().'/images/facility-icon.png" 
title="playground facility" 
content="Sed scelerisque tincidunt est venenatis laoreet. Nam eget enim velit. Curabitur nisi massa, sagittis sit amet libero in, lacinia facilisis dolor. Sed et enim vel dolor tempus viverra. Suspendisse elit lorem, vehicula vel tempor eget"]

[infolist
thumb="'.get_template_directory_uri().'/images/expert-teachers-icon.png"
title="Expert Teachers"
content="Sed scelerisque tincidunt est venenatis laoreet. Nam eget enim velit. Curabitur nisi massa, sagittis sit amet libero in, lacinia facilisis dolor. Sed et enim vel dolor tempus viverra. Suspendisse elit lorem, vehicula vel tempor eget"]

[infolist
thumb="'.get_template_directory_uri().'/images/sessions-icon.png"
title="Full Day Sessions" 
content="Sed scelerisque tincidunt est venenatis laoreet. Nam eget enim velit. Curabitur nisi massa, sagittis sit amet libero in, lacinia facilisis dolor. Sed et enim vel dolor tempus viverra. Suspendisse elit lorem, vehicula vel tempor eget"]
		[/su_column] [/su_row]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section1_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections1',
				'settings' => 'complete[section1_content]',
			)) );	

// Section 1 Background Color
	$wp_customize->add_setting( 'complete[section1_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section1_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections1',
		'settings' => 'complete[section1_bgcolor_id]',
	) ) );

// Section1 Background Image
	$wp_customize->add_setting( 'complete[section1_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section1_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections1',
			'settings'    => 'complete[section1_bg_image]'
				)
			)
	);
	
// Section1 Background Video
	$wp_customize->add_setting( 'complete[section1_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section1_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections1',
			'settings'    => 'complete[section1_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section1]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section1', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections1',
		'settings' => 'complete[hide_boxes_section1]',
	)) );	 
//----------------------HOME SECTION 1----------------------------------

//----------------------HOME SECTION 2----------------------------------

	$wp_customize->add_setting('complete[section2_title]', array(
		'type' => 'option',
		'default'	=> __('Our School Gallery','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section2_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections2',
		'settings' => 'complete[section2_title]',
	)) );	
	
$wp_customize->add_setting('complete[section2_content]', array(
	'type' => 'option',
	'default' => '[photogallery filter="false"]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section2_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections2',
				'settings' => 'complete[section2_content]',
			)) );	

// Section 2 Background Color
	$wp_customize->add_setting( 'complete[section2_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#fdfcf7',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section2_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections2',
		'settings' => 'complete[section2_bgcolor_id]',
	) ) );

// Section2 Background Image
	$wp_customize->add_setting( 'complete[section2_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section2_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections2',
			'settings'    => 'complete[section2_bg_image]'
				)
			)
	);
	
// Section2 Background Video
	$wp_customize->add_setting( 'complete[section2_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section2_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections2',
			'settings'    => 'complete[section2_bg_video]'
				)
			)
	);		
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section2]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section2', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections2',
		'settings' => 'complete[hide_boxes_section2]',
	)) );
//----------------------HOME SECTION 2----------------------------------

//----------------------HOME SECTION 3----------------------------------

	$wp_customize->add_setting('complete[section3_title]', array(
		'type' => 'option',
		'default'	=> __('Featured Courses','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section3_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections3',
		'settings' => 'complete[section3_title]',
	)) );	
	
$wp_customize->add_setting('complete[section3_content]', array(
	'type' => 'option',
	'default' => '[courses-box col="3" show="3" catslug="" excerptlength="30"]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section3_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections3',
				'settings' => 'complete[section3_content]',
			)) );	

// Section 3 Background Color
	$wp_customize->add_setting( 'complete[section3_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section3_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections3',
		'settings' => 'complete[section3_bgcolor_id]',
	) ) );

// Section3 Background Image
	$wp_customize->add_setting( 'complete[section3_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section3_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections3',
			'settings'    => 'complete[section3_bg_image]'
				)
			)
	);
	
// Section3 Background Video
	$wp_customize->add_setting( 'complete[section3_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section3_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections3',
			'settings'    => 'complete[section3_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section3]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section3', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections3',
		'settings' => 'complete[hide_boxes_section3]',
	)) );	
//----------------------HOME SECTION 3----------------------------------

//----------------------HOME SECTION 4----------------------------------

	$wp_customize->add_setting('complete[section4_title]', array(
		'type' => 'option',
		'default'	=> __('Our Teachers','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section4_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections4',
		'settings' => 'complete[section4_title]',
	)) );	
	
$wp_customize->add_setting('complete[section4_content]', array(
	'type' => 'option',
	'default' => '[ourteachers col="2" show="4" excerptlength="25"]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section4_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections4',
				'settings' => 'complete[section4_content]',
			)) );	

// Section 4 Background Color
	$wp_customize->add_setting( 'complete[section4_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#fdfcf7',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section4_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections4',
		'settings' => 'complete[section4_bgcolor_id]',
	) ) );

// Section4 Background Image
	$wp_customize->add_setting( 'complete[section4_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section4_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections4',
			'settings'    => 'complete[section4_bg_image]'
				)
			)
	);
	
// Section4 Background Video
	$wp_customize->add_setting( 'complete[section4_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section4_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections4',
			'settings'    => 'complete[section4_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section4]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section4', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections4',
		'settings' => 'complete[hide_boxes_section4]',
	)) );		
//----------------------HOME SECTION 4----------------------------------

//----------------------HOME SECTION 5----------------------------------

	$wp_customize->add_setting('complete[section5_title]', array(
		'type' => 'option',
		'default'	=> __('Tabs','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section5_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections5',
		'settings' => 'complete[section5_title]',
	)) );	
	
$wp_customize->add_setting('complete[section5_content]', array(
	'type' => 'option',
	'default' => ' [su_row]
[su_column size="1/2"]
<h3>Default tab</h3>
[su_tabs]
[su_tab title="Tab 1"] Tab 1 content [/su_tab]
[su_tab title="Tab 2"] Tab 2 content [/su_tab]
[su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs][/su_column]
[su_column size="1/2"]
<h3>Custom active tab</h3>
[su_tabs active="2"]
[su_tab title="Tab 1"] Tab 1 content [/su_tab]
[su_tab title="Tab 2"] Tab 2 content [/su_tab]
[su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs][/su_column]
[/su_row]

[su_row]
[su_column size="1/2"]
<h3>Vertical tabs</h3>
[su_tabs vertical="yes"]
  [su_tab title="Tab 1"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs]
[/su_column]

[su_column size="1/2"]
<h3>Tabs anchors</h3>
[su_tabs]
  [su_tab title="Tab 1" anchor="First"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2" anchor="Second"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3" anchor="Third"] Tab 3 content [/su_tab]
[/su_tabs]

Use next links to switch this tabs

<a href="#First">Open tab 1</a> | <a href="#Second">Open tab 2</a> | <a href="#Third">Open tab 3</a>
[/su_column]
[/su_row]

[su_row]
[su_column size="1/2"]
<h3>Disabled tabs</h3>
[su_tabs]
  [su_tab title="Tab 1"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3"] Tab 3 content [/su_tab]
  [su_tab title="Tab 4 (disabled)" disabled="yes"] Tab 4 content [/su_tab]
  [su_tab title="Tab 5 (disabled)" disabled="yes"] Tab 5 content [/su_tab]
[/su_tabs]
[/su_column]
[su_column size="1/2"]
<h3>Custom styles</h3>
[su_tabs class="my-custom-tabs"]
  [su_tab title="Tab 1"] Tab 1 content [/su_tab]
  [su_tab title="Tab 2"] Tab 2 content [/su_tab]
  [su_tab title="Tab 3"] Tab 3 content [/su_tab]
[/su_tabs]
[/su_column]
[/su_row]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section5_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections5',
				'settings' => 'complete[section5_content]',
			)) );	

// Section 5 Background Color
	$wp_customize->add_setting( 'complete[section5_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section5_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections5',
		'settings' => 'complete[section5_bgcolor_id]',
	) ) );

// Section5 Background Image
	$wp_customize->add_setting( 'complete[section5_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section5_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections5',
			'settings'    => 'complete[section5_bg_image]'
				)
			)
	);
	
	
// Section5 Background Video
	$wp_customize->add_setting( 'complete[section5_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section5_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections5',
			'settings'    => 'complete[section5_bg_video]'
				)
			)
	);		
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section5]',array(
			'type' => 'option',
			'default' => '1',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section5', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections5',
		'settings' => 'complete[hide_boxes_section5]',
	)) );		
//----------------------HOME SECTION 5----------------------------------

//----------------------HOME SECTION 6----------------------------------

	$wp_customize->add_setting('complete[section6_title]', array(
		'type' => 'option',
		'default'	=> __('','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section6_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections6',
		'settings' => 'complete[section6_title]',
	)) );	
	
$wp_customize->add_setting('complete[section6_content]', array(
	'type' => 'option',
	'default' => '[testimonials-rotator show="3"]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section6_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections6',
				'settings' => 'complete[section6_content]',
			)) );	

// Section 6 Background Color
	$wp_customize->add_setting( 'complete[section6_bgcolor_id]', array(
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section6_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections6',
		'settings' => 'complete[section6_bgcolor_id]',
	) ) );

// Section6 Background Image
	$wp_customize->add_setting( 'complete[section6_bg_image]',array( 
		'type' => 'option',
		'default' => ''.get_template_directory_uri().'/images/testimonial-section-bg.jpg',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section6_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections6',
			'settings'    => 'complete[section6_bg_image]'
				)
			)
	);
	
	
// Section6 Background Video
	$wp_customize->add_setting( 'complete[section6_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section6_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections6',
			'settings'    => 'complete[section6_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section6]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section6', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections6',
		'settings' => 'complete[hide_boxes_section6]',
	)) );		
//----------------------HOME SECTION 6----------------------------------

//----------------------HOME SECTION 7----------------------------------

	$wp_customize->add_setting('complete[section7_title]', array(
		'type' => 'option',
		'default'	=> __('Latest Blog Posts','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section7_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections7',
		'settings' => 'complete[section7_title]',
	)) );	
	
$wp_customize->add_setting('complete[section7_content]', array(
	'type' => 'option',
	'default' => '[posts-style3 show="3" cat="1" excerptlength="24"] ',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section7_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections7',
				'settings' => 'complete[section7_content]',
			)) );	

// Section 7 Background Color
	$wp_customize->add_setting( 'complete[section7_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#fdfcf7',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section7_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections7',
		'settings' => 'complete[section7_bgcolor_id]',
	) ) );

// Section7 Background Image
	$wp_customize->add_setting( 'complete[section7_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section7_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections7',
			'settings'    => 'complete[section7_bg_image]'
				)
			)
	);
	
		
// Section7 Background Video
	$wp_customize->add_setting( 'complete[section7_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section7_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections7',
			'settings'    => 'complete[section7_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section7]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section7', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections7',
		'settings' => 'complete[hide_boxes_section7]',
	)) );	
//----------------------HOME SECTION 7----------------------------------

//----------------------HOME SECTION 8----------------------------------

	$wp_customize->add_setting('complete[section8_title]', array(
		'type' => 'option',
		'default'	=> __('Features','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section8_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections8',
		'settings' => 'complete[section8_title]',
	)) );	
	
$wp_customize->add_setting('complete[section8_content]', array(
	'type' => 'option',
	'default' => '[posts-timeline show="12" cat="1" excerptlength="28"]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section8_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections8',
				'settings' => 'complete[section8_content]',
			)) );	

// Section 8 Background Color
	$wp_customize->add_setting( 'complete[section8_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#f2f2f2',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section8_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections8',
		'settings' => 'complete[section8_bgcolor_id]',
	) ) );

// Section8 Background Image
	$wp_customize->add_setting( 'complete[section8_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section8_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections8',
			'settings'    => 'complete[section8_bg_image]'
				)
			)
	);
	
// Section8 Background Video
	$wp_customize->add_setting( 'complete[section8_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section8_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections8',
			'settings'    => 'complete[section8_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section8]',array(
			'type' => 'option',
			'default' => '1',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section8', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections8',
		'settings' => 'complete[hide_boxes_section8]',
	)) );	
//----------------------HOME SECTION 8----------------------------------

//----------------------HOME SECTION 9----------------------------------

	$wp_customize->add_setting('complete[section9_title]', array(
		'type' => 'option',
		'default'	=> __('Toggle & Skills','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section9_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections9',
		'settings' => 'complete[section9_title]',
	)) );	
	
$wp_customize->add_setting('complete[section9_content]', array(
	'type' => 'option',
	'default' => '[su_row]
  [su_column size="1/2"]
  [su_accordion]
  [su_spoiler title="Vision and Mission" open="yes"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique fermentum ullamcorper. Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet.[/su_spoiler]
  [su_spoiler title="Company Philopsphy"]Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique fermentum ullamcorper. [/su_spoiler]
  [su_spoiler title="Spoiler title"]Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique fermentum ullamcorper. Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit.[/su_spoiler]
[/su_accordion]
  [/su_column]
  [su_column size="1/2"] [skill title="HTML" percent="80" bgcolor="#ff7800"][skill title="Wordpress" percent="99" bgcolor="#ff7800"][skill title="Web Design" percent="90" bgcolor="#ff7800"][skill title="Web Development" percent="95" bgcolor="#ff7800"][skill title="Responsive" percent="85" bgcolor="#ff7800"] [/su_column]
[/su_row]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section9_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections9',
				'settings' => 'complete[section9_content]',
			)) );	

// Section 9 Background Color
	$wp_customize->add_setting( 'complete[section9_bgcolor_id]', array(
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section9_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections9',
		'settings' => 'complete[section9_bgcolor_id]',
	) ) );

// Section9 Background Image
	$wp_customize->add_setting( 'complete[section9_bg_image]',array( 
		'type' => 'option',
		'default' => ''.get_template_directory_uri().'/images/skills-bg.jpg',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section9_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections9',
			'settings'    => 'complete[section9_bg_image]'
				)
			)
	);
	
// Section9 Background Video
	$wp_customize->add_setting( 'complete[section9_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section9_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections9',
			'settings'    => 'complete[section9_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section9]',array(
			'type' => 'option',
			'default' => '1',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section9', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections9',
		'settings' => 'complete[hide_boxes_section9]',
	)) );	
//----------------------HOME SECTION 9----------------------------------

//----------------------HOME SECTION 10----------------------------------

	$wp_customize->add_setting('complete[section10_title]', array(
		'type' => 'option',
		'default'	=> __('List Type','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section10_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections10',
		'settings' => 'complete[section10_title]',
	)) );	
	
$wp_customize->add_setting('complete[section10_content]', array(
	'type' => 'option',
	'default' => '[su_row]
  [su_column size="1/3"] [su_list icon="icon: arrow-circle-right" icon_color="#fec42b"]
<ul>
	<li>Lorem ipsum dolor sit amet</li>
	<li>Habitant morbi tristique senectus et</li>
	<li>Suspendisse ut interdum risus</li>
	<li>Pellentesque mi at blandit</li>
</ul>
[/su_list]

[su_list icon="icon: check" icon_color="#ce7b4e"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list][/su_column]
  [su_column size="1/3"] [su_list icon="icon: long-arrow-right" icon_color="#fec42b"]<ul>
	<li>Lorem ipsum dolor sit amet</li>
	<li>Habitant morbi tristique senectus et</li>
	<li>Suspendisse ut interdum risus</li>
	<li>Pellentesque mi at blandit</li>
</ul>[/su_list]

[su_list icon="icon: arrow-circle-right" icon_color="#fec42b"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list]

[/su_column]

[su_column size="1/3"]
	[su_list icon="icon: hand-o-right" icon_color="#e15453"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list]

 [su_list icon="icon: plane" icon_color="#53e1d9"]<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Habitant morbi tristique senectus et</li>
<li>Suspendisse ut interdum risus</li>
<li>Pellentesque mi at blandit</li>
</ul>[/su_list]
	
[/su_column]
[/su_row]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section10_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections10',
				'settings' => 'complete[section10_content]',
			)) );	

// Section 10 Background Color
	$wp_customize->add_setting( 'complete[section10_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section10_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections10',
		'settings' => 'complete[section10_bgcolor_id]',
	) ) );

// Section10 Background Image
	$wp_customize->add_setting( 'complete[section10_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section10_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections10',
			'settings'    => 'complete[section10_bg_image]'
				)
			)
	);
	
// Section10 Background Video
	$wp_customize->add_setting( 'complete[section10_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section10_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections10',
			'settings'    => 'complete[section10_bg_video]'
				)
			)
	);		
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section10]',array(
			'type' => 'option',
			'default' => '1',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section10', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections10',
		'settings' => 'complete[hide_boxes_section10]',
	)) );		
//----------------------HOME SECTION 10----------------------------------

//----------------------HOME SECTION 11----------------------------------

	$wp_customize->add_setting('complete[section11_title]', array(
		'type' => 'option',
		'default'	=> __('Our Partners','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'section11_title', array( 
		'type' => 'text',
		'label'	=> __('Section Title','play-school'),
		'section' => 'home_sections11',
		'settings' => 'complete[section11_title]',
	)) );	
	
$wp_customize->add_setting('complete[section11_content]', array(
	'type' => 'option',
	'default' => ' [su_row]
  [su_column size="2/5"]
  <h1>H1 Heading Site Title</h1>
  <h2>H2 Heading Site Title</h2>
  <h3>H3 Heading Site Title</h3>
  <h4>H4 Heading Site Title</h4>
  <h5>H5 Heading Site Title</h5>
  <h6>H6 Heading Site Title</h6>
  [/su_column]
  [su_column size="3/5"] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quam nibh, euismod eget nulla a, tempor scelerisque lorem. Proin dignissim arcu tristique <a href="#">fermentum ullamcorper</a>. Integer lacinia scelerisque enim eu pretium. Nam elementum turpis orci, ac porttitor diam suscipit sit amet. [/su_column]
[/su_row][su_spacer size="10"]<h2>Our Clients</h2>[client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"][client url="#" image="'. get_template_directory_uri().'/images/client.jpg"]',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new complete_Editor_Control( $wp_customize, 'section11_content', array( 
				'type' => 'editor',
				'label' => __('Section Content','play-school'), 
				'section' => 'home_sections11',
				'settings' => 'complete[section11_content]',
			)) );	

// Section 11 Background Color
	$wp_customize->add_setting( 'complete[section11_bgcolor_id]', array(
		'type' => 'option',
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'section11_bgcolor_id', array(
		'label' => __('Section Background Color','play-school'),
		'section' => 'home_sections11',
		'settings' => 'complete[section11_bgcolor_id]',
	) ) );

// Section11 Background Image
	$wp_customize->add_setting( 'complete[section11_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'section11_bg_image',array(
			'label'       => __( 'Section Background Image', 'play-school' ),
			'section'     => 'home_sections11',
			'settings'    => 'complete[section11_bg_image]'
				)
			)
	);
	
// Section11 Background Video
	$wp_customize->add_setting( 'complete[section11_bg_video]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Text_Control( $wp_customize, 'section11_bg_video',array(
			'label'       => __( 'Section Background Video Url .mp4 Only', 'play-school' ),
			'section'     => 'home_sections11',
			'settings'    => 'complete[section11_bg_video]'
				)
			)
	);	
	
// Hide Section
	$wp_customize->add_setting('complete[hide_boxes_section11]',array(
			'type' => 'option',
			'default' => '1',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_boxes_section11', array(
		'label' => __('Hide This Section','play-school'),
		'section' => 'home_sections11',
		'settings' => 'complete[hide_boxes_section11]',
	)) );	
//----------------------HOME SECTION 11----------------------------------
 
//----------------------FRONT CONTENT SECTION----------------------------------
	$ImageUrl1 = esc_url(get_template_directory_uri() ."/images/slides/slider1.jpg");
	$ImageUrl2 = esc_url(get_template_directory_uri() ."/images/slides/slider2.jpg");
	$ImageUrl3 = esc_url(get_template_directory_uri() ."/images/slides/slider3.jpg");
//----------------------SLIDER TYPE SECTION----------------------------------

//SLIDER TYPE
$wp_customize->add_setting( 'complete[slider_type_id]', array(
		'type' => 'option',
        'default' => 'static',
		'sanitize_callback' => 'complete_sanitize_choices',
) );
 
			$wp_customize->add_control('slider_type_id', array(
					'type' => 'select',
					'label' => __('Slider Type *','play-school'),
					'description' => __('Choose the Slider type.','play-school'),
					'section' => 'slider_section',
					'choices' => array(
						'static'=> __('Default Slider', 'play-school'),
						'customslider'=> __('Custom Slider', 'play-school'),
						'noslider'=>__('Disable Slider', 'play-school')
					),
					'settings'    => 'complete[slider_type_id]'
			) );


//----------------------DEFAULT SLIDER SECTION----------------------------------

// Slide Font Typography And Colors

// Slide Title Font Family
$wp_customize->add_setting( 'complete[sldtitle_font_id][font-family]', array(
	'type' => 'option',
	'default' => 'Montserrat',
	'sanitize_callback' => 'esc_attr',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('sldtitle_font_family', array(
					'type' => 'select',
					'label' => __('Slide Title Family','play-school'),
					'section' => 'slider_section',
					'settings' => 'complete[sldtitle_font_id][font-family]',
					'choices' => customizer_library_get_font_choices(),
			) );

// Slide Title Font Subsets
$wp_customize->add_setting( 'complete[sldtitle_font_id][subsets]', array(
	'type' => 'option',
	'default' => 'latin',
	'sanitize_callback' => 'esc_attr',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('sldtitle_font_subsets', array(
					'type' => 'select',
					'label' => __('Slide Title Subsets','play-school'),
					'section' => 'slider_section',
					'settings' => 'complete[sldtitle_font_id][subsets]',
					'choices' => customizer_library_get_google_font_subsets(),
			) );

// Slide Description Font Family
$wp_customize->add_setting( 'complete[slddesc_font_id][font-family]', array(
	'type' => 'option',
	'default' => 'Roboto',
	'sanitize_callback' => 'esc_attr',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('slddesc_font_family', array(
					'type' => 'select',
					'label' => __('Slide Description Family','play-school'),
					'section' => 'slider_section',
					'settings' => 'complete[slddesc_font_id][font-family]',
					'choices' => customizer_library_get_font_choices(),
			) );

// Slide Descripotion Font Subsets
$wp_customize->add_setting( 'complete[slddesc_font_id][subsets]', array(
	'type' => 'option',
	'default' => 'latin',
	'sanitize_callback' => 'esc_attr',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('slddesc_font_subsets', array(
					'type' => 'select',
					'label' => __('Slide Description Subsets','play-school'),
					'section' => 'slider_section',
					'settings' => 'complete[slddesc_font_id][subsets]',
					'choices' => customizer_library_get_google_font_subsets(),
			) );

// Slide Button Font Family
$wp_customize->add_setting( 'complete[sldbtn_font_id][font-family]', array(
	'type' => 'option',
	'default' => 'Lato',
	'sanitize_callback' => 'esc_attr',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('sldbtn_font_family', array(
					'type' => 'select',
					'label' => __('Slide Button Family','play-school'),
					'section' => 'slider_section',
					'settings' => 'complete[sldbtn_font_id][font-family]',
					'choices' => customizer_library_get_font_choices(),
			) );

// Slide Button Font Subsets
$wp_customize->add_setting( 'complete[sldbtn_font_id][subsets]', array(
	'type' => 'option',
	'default' => 'latin',
	'sanitize_callback' => 'esc_attr',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('sldbtn_font_subsets', array(
					'type' => 'select',
					'label' => __('Slide Button Subsets','play-school'),
					'section' => 'slider_section',
					'settings' => 'complete[sldbtn_font_id][subsets]',
					'choices' => customizer_library_get_google_font_subsets(),
			) );

// Slide Title Font Size
$wp_customize->add_setting('complete[sldtitle_font_id][font-size]', array(
	'type' => 'option',
	'default' => '36px',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('sldtitle_font_size', array(
				'type' => 'text',
				'label' => __('Slide Title Font Size','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[sldtitle_font_id][font-size]',
						'input_attrs'	=> array(
							'class'	=> 'mini_control',
						)
			) );
			
// Slide Description Font Size
$wp_customize->add_setting('complete[slddesc_font_id][font-size]', array(
	'type' => 'option',
	'default' => '14px',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('slddesc_font_size', array(
				'type' => 'text',
				'label' => __('Slide Description Font Size','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[slddesc_font_id][font-size]',
						'input_attrs'	=> array(
							'class'	=> 'mini_control',
						)
			) );

// Slide Button Font Size
$wp_customize->add_setting('complete[sldbtn_font_id][font-size]', array(
	'type' => 'option',
	'default' => '14px',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('sldbtn_font_size', array(
				'type' => 'text',
				'label' => __('Slide Button Font Size','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[sldbtn_font_id][font-size]',
						'input_attrs'	=> array(
							'class'	=> 'mini_control',
						)
			) );
			
// Slide Title Color
$wp_customize->add_setting( 'complete[slidetitle_color_id]', array(
	'type' => 'option',
	'default' => '#4b4c47',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'slidetitle_color_id', array(
				'label' => __('Slide Title Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[slidetitle_color_id]',
			) ) );
			
// Slide Description Color
$wp_customize->add_setting( 'complete[slddesc_color_id]', array(
	'type' => 'option',
	'default' => '#696969',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'slddesc_color_id', array(
				'label' => __('Slide Description Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[slddesc_color_id]',
			) ) );	
			
// Slide Button Text Color
$wp_customize->add_setting( 'complete[sldbtntext_color_id]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sldbtntext_color_id', array(
				'label' => __('Slide Button Text Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[sldbtntext_color_id]',
			) ) );			
			
// Slide Button Background Color
$wp_customize->add_setting( 'complete[sldbtn_color_id]', array(
	'type' => 'option',
	'default' => '#669d89',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sldbtn_color_id', array(
				'label' => __('Slide Button Background Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[sldbtn_color_id]',
			) ) );
			
// Slide BUtton Hoover Bg Color
$wp_customize->add_setting( 'complete[sldbtn_hvcolor_id]', array(
	'type' => 'option',
	'default' => '#ea5330',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sldbtn_hvcolor_id', array(
				'label' => __('Slide Button Hover Background Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[sldbtn_hvcolor_id]',
			) ) );	
			
			
// Slide Pager Color
/*$wp_customize->add_setting( 'complete[slide_pager_color_id]', array(
	'type' => 'option',
	'default' => '#ea5330',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'slide_pager_color_id', array(
				'label' => __('Slide Pager Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[slide_pager_color_id]',
			) ) );*/		
			
// Slide Active Pager Color
/*$wp_customize->add_setting( 'complete[slide_active_pager_color_id]', array(
	'type' => 'option',
	'default' => '#000000',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'slide_active_pager_color_id', array(
				'label' => __('Slide Active Pager Color','play-school'),
				'section' => 'slider_section',
				'settings' => 'complete[slide_active_pager_color_id]',
			) ) );*/				
												

// Slide Font Typography And Colors
	
	// Slide 1 Start
	$wp_customize->add_setting( 'complete[slide_image1]',array( 
		'type' => 'option',
		'default' => $ImageUrl1,
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image1',array(
			'label'       => __( 'Slide Image 1', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image1]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title1]', array(
		'type' => 'option',
		'default'	=> __('LEARNING CAN BE FUN<br><span style="color:#ea5330;">BEST PLACE FOR CHILDREN</span>','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title1', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 1','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title1]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc1]', array(
		'type' => 'option',
		'default'	=> __('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc1', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 1','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc1]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link1]', array(
		'type' => 'option',
		'default'	=> __('#link1','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link1', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 1','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link1]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn1]', array(
		'type' => 'option',
		'default'	=> __('Read More','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn1', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 1','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn1]',
	)) );	
	// Slide 1 End
	
	// Slide 2 Start
	$wp_customize->add_setting( 'complete[slide_image2]',array( 
		'type' => 'option',
		'default' => $ImageUrl2,
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image2',array(
			'label'       => __( 'Slide Image 2', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image2]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title2]', array(
		'type' => 'option',
		'default'	=> __('IMPARTING BEST EDUCATION<br><span style="color:#ea5330;">BEST TEACHING SINCE AGES</span>','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title2', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 2','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title2]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc2]', array(
		'type' => 'option',
		'default'	=> __('Industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the.','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc2', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 2','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc2]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link2]', array(
		'type' => 'option',
		'default'	=> __('#link2','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link2', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 2','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link2]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn2]', array(
		'type' => 'option',
		'default'	=> __('Read More','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn2', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 2','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn2]',
	)) );	
	// Slide 2 End
	
	// Slide 3 Start
	$wp_customize->add_setting( 'complete[slide_image3]',array( 
		'type' => 'option',
		'default' => $ImageUrl3,
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image3',array(
			'label'       => __( 'Slide Image 3', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image3]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title3]', array(
		'type' => 'option',
		'default'	=> __('CREATIVE LEARNING METHODS<br><span style="color:#ea5330;">MODERN TOOLS OF LEARNING</span>','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title3', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 3','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title3]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc3]', array(
		'type' => 'option',
		'default'	=> __('printing and typesetting industry. Lorem Ipsum has been the Industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum is simply dummy text of the.','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc3', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 3','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc3]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link3]', array(
		'type' => 'option',
		'default'	=> __('#link3','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link3', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 3','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link3]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn3]', array(
		'type' => 'option',
		'default'	=> __('Read More','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn3', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 3','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn3]',
	)) );	
	// Slide 3 End
	// Slide 4 Start
	$wp_customize->add_setting( 'complete[slide_image4]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image4',array(
			'label'       => __( 'Slide Image 4', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image4]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title4]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title4', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 4','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title4]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc4]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc4', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 4','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc4]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link4]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link4', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 4','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link4]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn4]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn4', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 4','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn4]',
	)) );	
	// Slide 4 End
	
	// Slide 5 Start
	$wp_customize->add_setting( 'complete[slide_image5]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image5',array(
			'label'       => __( 'Slide Image 5', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image5]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title5]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title5', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 5','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title5]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc5]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc5', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 5','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc5]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link5]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link5', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 5','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link5]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn5]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn5', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 5','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn5]',
	)) );	
	// Slide 5 End
	// Slide 6 Start
	$wp_customize->add_setting( 'complete[slide_image6]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image6',array(
			'label'       => __( 'Slide Image 6', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image6]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title6]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title6', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 6','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title6]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc6]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc6', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 6','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc6]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link6]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link6', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 6','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link6]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn6]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn6', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 6','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn6]',
	)) );	
	// Slide 6 End
	// Slide 7 Start
	$wp_customize->add_setting( 'complete[slide_image7]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image7',array(
			'label'       => __( 'Slide Image 7', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image7]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title7]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title7', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 7','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title7]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc7]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc7', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 7','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc7]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link7]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link7', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 7','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link7]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn7]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn7', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 7','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn7]',
	)) );	
	// Slide 7 End
	// Slide 8 Start
	$wp_customize->add_setting( 'complete[slide_image8]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image8',array(
			'label'       => __( 'Slide Image 8', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image8]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title8]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title8', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 8','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title8]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc8]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc8', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 8','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc8]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link8]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link8', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 8','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link8]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn8]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn8', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 8','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn8]',
	)) );	
	// Slide 8 End
	// Slide 9 Start
	$wp_customize->add_setting( 'complete[slide_image9]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image9',array(
			'label'       => __( 'Slide Image 9', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image9]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title9]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title9', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 9','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title9]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc9]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc9', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 9','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc9]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link9]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link9', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 9','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link9]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn9]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn9', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 9','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn9]',
	)) );	
	// Slide 9 End
	
	// Slide 10 Start
	$wp_customize->add_setting( 'complete[slide_image10]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slide_image10',array(
			'label'       => __( 'Slide Image 10', 'play-school' ),
			'section'     => 'slider_section',
			'settings'    => 'complete[slide_image10]',
				)
			)
	);
	
	$wp_customize->add_setting('complete[slide_title10]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_title10', array( 
		'type' => 'text',
		'label'	=> __('Slide Title 10','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_title10]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_desc10]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'slide_desc10', array( 
		'type' => 'textarea',
		'label'	=> __('Slide Description 10','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_desc10]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_link10]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_link10', array( 
		'type' => 'text',
		'label'	=> __('Slide Link 10','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_link10]',
	)) );	
	
	$wp_customize->add_setting('complete[slide_btn10]', array(
		'type' => 'option',
		'default'	=> '',
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'slide_btn10', array( 
		'type' => 'text',
		'label'	=> __('Slide Button 10','play-school'),
		'section' => 'slider_section',
		'settings' => 'complete[slide_btn10]',
	)) );	
	// Slide 10 End									
 
//----------------------CUSTOM SLIDER SECTION----------------------------------	

$wp_customize->add_setting('complete[custom_slider]', array(
	'type' => 'option',
	'default' => '',
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'custom_slider', array( 
				'type' => 'textarea',
				'label' => __('Custom Slider Shortcode','play-school'), 
				'section' => 'slider_section',
				'settings' => 'complete[custom_slider]',
			)) );		

//---------------SLIDER CALLBACK-------------------//
function complete_slider_static( $control ) {
    $layout_setting = $control->manager->get_setting('complete[slider_type_id]')->value();
     
    if ( $layout_setting == 'static' ) return true;
     
    return false;
}
function complete_slider_nivoacc( $control ) {
    $layout_setting = $control->manager->get_setting('complete[slider_type_id]')->value();
     
    if ( $layout_setting == 'accordion' || $layout_setting == 'nivo' ) return true;
     
    return false;
}