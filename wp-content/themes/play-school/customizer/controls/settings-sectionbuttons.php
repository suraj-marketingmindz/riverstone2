<?php
//----------------------Featured Block Button----------------------------------

	$wp_customize->add_setting('complete[featured_block_button]', array(
		'type' => 'option',
		'default'	=> __('Read More','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'featured_block_button', array( 
		'type' => 'text',
		'label'	=> __('Featured Block Button Text','play-school'),
		'section' => 'buttons_section',
		'settings' => 'complete[featured_block_button]',
	)) );	
	
//----------------------Recent Posts Block Button Text----------------------------

	$wp_customize->add_setting('complete[recentpost_block_button]', array(
		'type' => 'option',
		'default'	=> __('Read More','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'recentpost_block_button', array( 
		'type' => 'text',
		'label'	=> __('Recent Post Block Button Text','play-school'),
		'section' => 'buttons_section',
		'settings' => 'complete[recentpost_block_button]',
	)) );	
	
	$wp_customize->add_setting('complete[courses_block_button]', array(
		'type' => 'option',
		'default'	=> __('Apply Now','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'courses_block_button', array( 
		'type' => 'text',
		'label'	=> __('Courses Block Button Text','play-school'),
		'section' => 'buttons_section',
		'settings' => 'complete[courses_block_button]',
	)) );	
	
	$wp_customize->add_setting('complete[teacher_block_button]', array(
		'type' => 'option',
		'default'	=> __('Send Email','play-school'),
		'sanitize_callback' => 'wp_kses_post',
		'transport' => 'postMessage',
	) );
	$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'teacher_block_button', array( 
		'type' => 'text',
		'label'	=> __('Our Teacher Button Text','play-school'),
		'section' => 'buttons_section',
		'settings' => 'complete[teacher_block_button]',
	)) );		
 
// All Buttons Background Color
$wp_customize->add_setting( 'complete[featured_block_button_bg]', array(
	'type' => 'option',
	'default' => '#669d89',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'featured_block_button_bg', array(
	'label' => __('Button Background Color','play-school'),
	'section' => 'buttons_section',
	'settings' => 'complete[featured_block_button_bg]',
) ) );
 
// All Buttons Hover Background Color
$wp_customize->add_setting( 'complete[featured_block_hover_button_bg]', array(
	'type' => 'option',
	'default' => '#ea5330',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'featured_block_hover_button_bg', array(
	'label' => __('Button Hover Background Color','play-school'),
	'section' => 'buttons_section',
	'settings' => 'complete[featured_block_hover_button_bg]',
) ) );