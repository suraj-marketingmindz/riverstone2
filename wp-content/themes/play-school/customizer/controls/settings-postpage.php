<?php

//----------------------SINGLE POST LAYOUT-----------------------------------
$wp_customize->add_setting('complete[single_post_layout_id]', array(
		'type' => 'option',
		'default'           => 'single_layout1',
		'sanitize_callback' => 'sanitize_key',
	)
);

// Add the heaeder layout control.
$wp_customize->add_control('single_post_layout_id',array(
			'type' => 'select',
			'label'    => esc_html__( 'Single Post Layout *', 'play-school' ),
			'section'  => 'singlelayout_section',
			'settings' => 'complete[single_post_layout_id]',
			'choices'  => array(
				'single_layout1' => __('Single Post Right Sidebar', 'play-school'), 
				'single_layout2' => __('Single Post Left Sidebar', 'play-school'),
				'single_layout3' => __('Single Post Full Width', 'play-school'),
				'single_layout4' => __('Single Post No Sidebar', 'play-school'),
		  )
  ) );

//----------------------SINGLE POST SECTION----------------------------------


//Single Post Meta
$wp_customize->add_setting('complete[post_info_id]', array(
	'type' => 'option',
	'default' => '1',
	'sanitize_callback' => 'complete_sanitize_checkbox',
	'transport' => 'postMessage',
) );
 
			$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'post_info_id', array(
				'label' => __('Show Post Info','play-school'),
				'section' => 'singlepost_section',
				'settings' => 'complete[post_info_id]',
			)) );


//NEXT/PREVIOUS Posts
$wp_customize->add_setting('complete[post_nextprev_id]', array(
	'type' => 'option',
	'default' => '1',
	'sanitize_callback' => 'complete_sanitize_checkbox',
	'transport' => 'postMessage',
) );
 
			$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'post_nextprev_id', array(
				'label' => __('Next and Previous Posts','play-school'),
				'description'  => __('Display Next and Previous Posts Under Single Post', 'play-school' ),
				'section' => 'singlepost_section',
				'settings' => 'complete[post_nextprev_id]',
			)) );


///Show Comments
$wp_customize->add_setting('complete[post_comments_id]', array(
	'type' => 'option',
	'default' => '1',
	'sanitize_callback' => 'complete_sanitize_checkbox',
	'transport' => 'postMessage',
) );
 
			$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'post_comments_id', array(
				'label' => __('Comments','play-school'),
				'description'  => __('Show/Hide Comments in Posts and Pages', 'play-school' ),
				'section' => 'singlepost_section',
				'settings' => 'complete[post_comments_id]',
			)) );



//----------------------PAGE HEADER SECTION----------------------------------

//Page Header Default Background color
$wp_customize->add_setting( 'complete[page_header_color]', array(
	'type' => 'option',
	'default' => '#b5503d',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_header_color', array(
				'label' => __('Page Header Background','play-school'),
				'section' => 'pageheader_section',
				'settings' => 'complete[page_header_color]',
			) ) );
			
// Page Header Background Image
	$wp_customize->add_setting( 'complete[pageheader_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pageheader_bg_image',array(
			'label'       => __( 'Page Header Background Image', 'play-school' ),
			'section'     => 'pageheader_section',
			'settings'    => 'complete[pageheader_bg_image]'
				)
			)
	);
	
// Hide Page Header
	$wp_customize->add_setting('complete[hide_pageheader]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_pageheader', array(
		'label' => __('Hide Page Header','play-school'),
		'section' => 'pageheader_section',
		'settings' => 'complete[hide_pageheader]',
	)) );
	
//----------------------POST HEADER SECTION----------------------------------

//Post Header Default Background color
$wp_customize->add_setting( 'complete[post_header_color]', array(
	'type' => 'option',
	'default' => '#b5503d',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'post_header_color', array(
				'label' => __('Post Header Background','play-school'),
				'section' => 'postheader_section',
				'settings' => 'complete[post_header_color]',
			) ) );
			
// Post Header Background Image
	$wp_customize->add_setting( 'complete[postheader_bg_image]',array( 
		'type' => 'option',
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'postheader_bg_image',array(
			'label'       => __( 'Posts Header Background Image', 'play-school' ),
			'section'     => 'postheader_section',
			'settings'    => 'complete[postheader_bg_image]'
				)
			)
	);
	
// Hide Post Header
	$wp_customize->add_setting('complete[hide_postheader]',array(
			'type' => 'option',
			'default' => '',
			'sanitize_callback' => 'complete_sanitize_checkbox',
			'transport' => 'postMessage',
	));	 

	$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'hide_postheader', array(
		'label' => __('Hide Post Header','play-school'),
		'section' => 'postheader_section',
		'settings' => 'complete[hide_postheader]',
	)) );					
//----------------------BLOG PAGE SECTION----------------------------------


/*GET LIST OF CATEGORIES*/
$layercats = get_categories(); 
$newList = array();
foreach($layercats as $category) {
	$newList[$category->term_id] = $category->cat_name;
}	
//BLOG CATEGORY SELECT
//Page Header Default Text color
$wp_customize->add_setting( 'complete[blog_cat_id]', array(
	'type' => 'option',
	'default' => '',
	'sanitize_callback' => 'complete_sanitize_multicheck'
) );

$wp_customize->add_control( new complete_Multicheck_Control( $wp_customize, 'blog_cat_id', array(
        'type' => 'multicheck',
        'label' => __('Display Blog Posts from selected Categories *','play-school'),
        'section' => 'blogpage_section',
        'choices' =>$newList,
		'settings'    => 'complete[blog_cat_id]'
)) );

//Blog Page Post Count
$wp_customize->add_setting('complete[blog_num_id]', array(
	'type' => 'option',
	'default' => '9',
	'sanitize_callback' => 'complete_sanitize_number',
) );
			$wp_customize->add_control('blog_num_id', array(
				'type' => 'text',
				'label' => __('Blog Page Posts Count *','play-school'),
				'section' => 'blogpage_section',
				'settings' => 'complete[blog_num_id]',
							'input_attrs'	=> array(
								'class'	=> 'mini_control',
							)
			) );

///Blog Page Thumbnails
$wp_customize->add_setting('complete[show_blog_thumb]', array(
	'type' => 'option',
	'default' => '1',
	'sanitize_callback' => 'complete_sanitize_checkbox',
) );
 
				$wp_customize->add_control( new complete_Controls_Toggle_Control( $wp_customize, 'show_blog_thumb', array(
					'label' => __('Blog Page Thumbnails *','play-school'),
					'section' => 'blogpage_section',
					'settings' => 'complete[show_blog_thumb]',
				)) );



//---------Post & Page Color SETTINGS---------------------	

//Post Title Color
$wp_customize->add_setting( 'complete[title_txt_color_id]', array(
	'type' => 'option',
	'default' => '#666666',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'title_txt_color_id', array(
				'label' => __('Single Post All Heading Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[title_txt_color_id]',
			) ) );
//---------SIDEBAR & WIDGET Color SETTINGS---------------------	

//Sidebar Widgets Background Color
$wp_customize->add_setting( 'complete[sidebar_color_id]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_color_id', array(
				'label' => __('Sidebar Widgets Background','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[sidebar_color_id]',
			) ) );
			
//Sidebar Widgets Border Color
$wp_customize->add_setting( 'complete[sidebarborder_color_id]', array(
	'type' => 'option',
	'default' => '#eeeff5',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebarborder_color_id', array(
				'label' => __('Sidebar Widgets Border Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[sidebarborder_color_id]',
			) ) );			


//Sidebar Widget Title Color
$wp_customize->add_setting( 'complete[sidebar_tt_color_id]', array(
	'type' => 'option',
	'default' => '#666666',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_tt_color_id', array(
				'label' => __('Sidebar Widget Title Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[sidebar_tt_color_id]',
			) ) );


//Sidebar Widget Text Color
$wp_customize->add_setting( 'complete[sidebartxt_color_id]', array(
	'type' => 'option',
	'default' => '#999999',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebartxt_color_id', array(
				'label' => __('Sidebar Widget Text Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[sidebartxt_color_id]',
			) ) );
			
//Sidebar Widget Link Color
$wp_customize->add_setting( 'complete[sidebarlink_color_id]', array(
	'type' => 'option',
	'default' => '#b5503d',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebarlink_color_id', array(
				'label' => __('Sidebar Widget Link Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[sidebarlink_color_id]',
			) ) );		
			
//Sidebar Widget Link Hover Color
$wp_customize->add_setting( 'complete[sidebarlink_hover_color_id]', array(
	'type' => 'option',
	'default' => '#999999',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebarlink_hover_color_id', array(
				'label' => __('Sidebar Widget Link Hover Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[sidebarlink_hover_color_id]',
			) ) );	
			
// Flipbox Front Bg Color
$wp_customize->add_setting( 'complete[flipbg_front_color_id]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'flipbg_front_color_id', array(
				'label' => __('Flip Box Front Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[flipbg_front_color_id]',
			) ) );
			
// Flipbox Back Bg Color
$wp_customize->add_setting( 'complete[flipbg_back_color_id]', array(
	'type' => 'option',
	'default' => '#f7f7f7',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'flipbg_back_color_id', array(
				'label' => __('Flip Box Back Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[flipbg_back_color_id]',
			) ) );
			
// Flipbox Front Border Color
$wp_customize->add_setting( 'complete[flipborder_front_color_id]', array(
	'type' => 'option',
	'default' => '#e0e0e0',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'flipborder_front_color_id', array(
				'label' => __('Flip Box Front Border Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[flipborder_front_color_id]',
			) ) );		
			
// Flipbox Back Border Color
$wp_customize->add_setting( 'complete[flipborder_back_color_id]', array(
	'type' => 'option',
	'default' => '#000000',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'flipborder_back_color_id', array(
				'label' => __('Flip Box Back Border Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[flipborder_back_color_id]',
			) ) );		
 
// Divider Color
$wp_customize->add_setting( 'complete[divider_color_id]', array(
	'type' => 'option',
	'default' => '#8c8b8b',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'divider_color_id', array(
				'label' => __('Divider Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[divider_color_id]',
			) ) );	
			
// Timeline Box Bg
$wp_customize->add_setting( 'complete[timebox_color_id]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timebox_color_id', array(
				'label' => __('Timeline Box Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[timebox_color_id]',
			) ) );	
			
// Timeline Box Border
$wp_customize->add_setting( 'complete[timeboxborder_color_id]', array(
	'type' => 'option',
	'default' => '#dedede',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timeboxborder_color_id', array(
				'label' => __('Timeline Box Border Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[timeboxborder_color_id]',
			) ) );		
			
//////////////////
// Grid Box Bg
$wp_customize->add_setting( 'complete[gridbox_color_id]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gridbox_color_id', array(
				'label' => __('Grid Box Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[gridbox_color_id]',
			) ) );	
			
// Grid Box Border
$wp_customize->add_setting( 'complete[gridboxborder_color_id]', array(
	'type' => 'option',
	'default' => '#cccccc',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gridboxborder_color_id', array(
				'label' => __('Grid Box Border Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[gridboxborder_color_id]',
			) ) );
// Courses Block

$wp_customize->add_setting( 'complete[courseblockbg1]', array(
	'type' => 'option',
	'default' => '#a1d042',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'courseblockbg1', array(
				'label' => __('Classes & Courses Block 1 Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[courseblockbg1]',
			) ) );
			
$wp_customize->add_setting( 'complete[courseblockbg2]', array(
	'type' => 'option',
	'default' => '#ffba41',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'courseblockbg2', array(
				'label' => __('Classes & Courses Block 2 Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[courseblockbg2]',
			) ) );	
			
$wp_customize->add_setting( 'complete[courseblockbg3]', array(
	'type' => 'option',
	'default' => '#f13d6d',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'courseblockbg3', array(
				'label' => __('Classes & Courses Block 3 Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[courseblockbg3]',
			) ) );	
			
$wp_customize->add_setting( 'complete[courseblockbg4]', array(
	'type' => 'option',
	'default' => '#1c3b9f',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'courseblockbg4', array(
				'label' => __('Classes & Courses Block 4 Background Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[courseblockbg4]',
			) ) );		
			
			
$wp_customize->add_setting( 'complete[courseblockcolor]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'courseblockcolor', array(
				'label' => __('Classes & Courses Block Text Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[courseblockcolor]',
			) ) );
			
$wp_customize->add_setting( 'complete[courseblockhovercolor]', array(
	'type' => 'option',
	'default' => '#000000',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'courseblockhovercolor', array(
				'label' => __('Classes & Courses Block Hover Text Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[courseblockhovercolor]',
			) ) );	
			
			
$wp_customize->add_setting( 'complete[workinghrscolors]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'workinghrscolors', array(
				'label' => __('Working Hours Text Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[workinghrscolors]',
			) ) );	
			
			
$wp_customize->add_setting( 'complete[workingbgheighlights]', array(
	'type' => 'option',
	'default' => '#f13e3e',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'workingbgheighlights', array(
				'label' => __('Working Heighlight Bg Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[workingbgheighlights]',
			) ) );		
			
$wp_customize->add_setting( 'complete[historyborder]', array(
	'type' => 'option',
	'default' => '#ff9c00',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'historyborder', array(
				'label' => __('History Bottom Border Color','play-school'),
				'section' => 'general_color_section',
				'settings' => 'complete[historyborder]',
			) ) );																		

/////
$wp_customize->add_setting( 'complete[tabstextcolor]', array(
	'type' => 'option',
	'default' => '#ffffff',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tabstextcolor', array(
			'label' => __('Tabs Text Color','play-school'),
			'section' => 'general_color_section',
			'settings' => 'complete[tabstextcolor]',
		) ) );

$wp_customize->add_setting( 'complete[tabscolor]', array(
	'type' => 'option',
	'default' => '#707070',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tabscolor', array(
			'label' => __('Tabs Background Color','play-school'),
			'section' => 'general_color_section',
			'settings' => 'complete[tabscolor]',
		) ) );
			
$wp_customize->add_setting( 'complete[activetabscolor]', array(
	'type' => 'option',
	'default' => '#ea5330',
	'sanitize_callback' => 'sanitize_hex_color',
	'transport' => 'postMessage',
) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'activetabscolor', array(
			'label' => __('Active Tabs Background Color','play-school'),
			'section' => 'general_color_section',
			'settings' => 'complete[activetabscolor]',
		) ) );			

//////

// Courses Block																			

//Sidebar Widget Title Font Size
$wp_customize->add_setting('complete[wgttitle_size_id]', array(
	'type' => 'option',
	'default' => '16px',
	'sanitize_callback' => 'sanitize_text_field',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control('wgttitle_size_id', array(
				'type' => 'text',
				'label' => __('Sidebar Widget Title Font Size','play-school'),
				'section' => 'postpage_color_section',
				'settings' => 'complete[wgttitle_size_id]',
			) );
			
			
//============================ Contact Page =================================

//Contact Title
$wp_customize->add_setting('complete[contact_title]', array(
	'type' => 'option',
	'default' => __('Contact Info','play-school'),
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'contact_title', array( 
				'type' => 'text',
				'label' => __('Contact Title','play-school'), 
				'section' => 'contactpage_section',
				'settings' => 'complete[contact_title]',
			)) );	
			
//Contact Address
$wp_customize->add_setting('complete[contact_address]', array(
	'type' => 'option',
	'default' => __('Donec ultricies mattis nulla Australia','play-school'),
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'contact_address', array( 
				'type' => 'textarea',
				'label' => __('Company Address','play-school'),  
				'section' => 'contactpage_section',
				'settings' => 'complete[contact_address]',
			)) );
			
//Contact Phone
$wp_customize->add_setting('complete[contact_phone]', array(
	'type' => 'option',
	'default' => __('0789 256 321','play-school'),
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'contact_phone', array( 
				'type' => 'text',
				'label' => __('Phone Number','play-school'), 
				'section' => 'contactpage_section',
				'settings' => 'complete[contact_phone]',
			)) );	
			
//Contact Email
$wp_customize->add_setting('complete[contact_email]', array(
	'type' => 'option',
	'default' => __('info@companyname.com','play-school'),
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'contact_email', array( 
				'type' => 'text',
				'label' => __('Email Address','play-school'), 
				'section' => 'contactpage_section',
				'settings' => 'complete[contact_email]',
			)) );	
			
//Company URL
$wp_customize->add_setting('complete[contact_company_url]', array(
	'type' => 'option',
	'default' => __('http://demo.com','play-school'),
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Text_Control( $wp_customize, 'contact_company_url', array( 
				'type' => 'text',
				'label' => __('Company URL with http://','play-school'), 
				'section' => 'contactpage_section',
				'settings' => 'complete[contact_company_url]',
			)) );		
			
//Google Map
$wp_customize->add_setting('complete[contact_google_map]', array(
	'type' => 'option',
	'default' => __('https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d336003.6066860609!2d2.349634820486094!3d48.8576730786213!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x040b82c3688c9460!2sParis%2C+France!5e0!3m2!1sen!2sin!4v1433482358672','play-school'),
	'sanitize_callback' => 'wp_kses_post',
	'transport' => 'postMessage',
) );
			$wp_customize->add_control(	new WP_Customize_Textarea_Control( $wp_customize, 'contact_google_map', array( 
				'type' => 'textarea',
				'label' => __('Google Map','play-school'),  
				'section' => 'contactpage_section',
				'settings' => 'complete[contact_google_map]',
			)) );														