<?php 
/**
 * Posts Layout 4 for Playschool
 *
 * Displays Divider Icons to Display Under Home Elements Title
 *
 * @package complete
 * 
 * @since complete 1.0
 */
global $play;?>

  <!--DIVIDER-->
  <?php if($play['divider_icon'] !== 'no_divider') { ?>
      <div class="complete_divider">
          <span class="div_left"></span>
          <span class="div_middle"><i class="fa <?php echo esc_attr($play['divider_icon']); ?>"></i></span>
          <span class="div_right"></span>
      </div>
   <?php } ?>