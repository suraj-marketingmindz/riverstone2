/*-----------------------------------------------------------------------------------*/
/* Play School Responsive WordPress Theme */
/*-----------------------------------------------------------------------------------*/

Theme Name      :   Play School
Theme URI       :   https://www.sktthemes.net/shop/education-wordpress-theme/
Version         :   pro1.0
Tested up to    :   WP 4.5.3
Author          :   SKT Themes
Author URI      :   http://www.sktthemes.net/

license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

/*-----------------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*-----------------------------------------------------------------------------------*/

email       :   support@sktthemes.com

/*-----------------------------------------------------------------------------------*/
/* Theme Resources */
/*-----------------------------------------------------------------------------------*/
Click on Documentation and Support.

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.
- jquery.nivo.slider.js is licensed under MIT.


2 - Roboto - https://www.google.com/fonts/specimen/Roboto   
	License: Distributed under the terms of the Apache License, version 2.0 	http://www.apache.org/licenses/

3 - Slider Images
	https://pixabay.com/en/boy-blond-child-young-caucasian-1022991/
	https://pixabay.com/en/boy-kid-child-young-childhood-1367969/
	https://pixabay.com/en/kid-children-baby-kiddie-summer-1365105/
 
	
4 - Other Images
	https://pixabay.com/en/boys-kids-children-happy-happiness-286245/
	https://pixabay.com/en/children-school-fun-pool-balls-808660/
	https://pixabay.com/en/children-school-fun-pool-balls-808663/
	https://pixabay.com/en/children-school-fun-pool-balls-808664/
	https://pixabay.com/en/kids-party-joy-playground-1212890/
	https://pixabay.com/en/kids-at-swing-daycare-playground-1185902/
	https://pixabay.com/en/small-child-heat-festival-gy%C5%91r-kid-994135/
	https://pixabay.com/en/beautiful-smile-girl-woman-happy-1274360/
	https://pixabay.com/en/beautiful-smile-girl-woman-happy-1274360/
	https://pixabay.com/en/girl-beauty-studio-seductive-1252995/
	https://pixabay.com/en/woman-model-female-girl-young-828953/
	https://pixabay.com/en/kindergarten-kids-preschool-care-90505/

5 - Social Icons
	Icons have used from Font Awesome Icons http://fortawesome.github.io/Font-Awesome/icons/
	

Licensed under GPL. Fonts licensed under SIL OFL 1.1 http://scripts.sil.org/OFL

For any help you can mail us at support[at]sktthemes.com