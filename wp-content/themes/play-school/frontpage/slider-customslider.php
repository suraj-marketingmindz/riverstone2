<?php global $play; ?>
<?php if(!empty($play['custom_slider']) || is_customize_preview() ) { ?>
<div class="slidercus">
<?php echo do_shortcode($play['custom_slider']); ?>
</div> 
<?php } ?>