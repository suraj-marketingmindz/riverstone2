<?php
global $play;  
$ImageUrl[] = esc_url(get_template_directory_uri() ."/images/slides/slider1.jpg");
$ImageUrl[] = esc_url(get_template_directory_uri() ."/images/slides/slider2.jpg");
$ImageUrl[] = esc_url(get_template_directory_uri() ."/images/slides/slider3.jpg"); 

for($i=1; $i<=10; $i++){
	if(!empty($play['slide_image'.$i])){
		$imgArr[] = $i;
	}
}
?>
<section id="home_slider">
  <div class="slider-main">
    <?php if(!empty($imgArr)){ ?>
    <div class="slider-wrapper theme-default">
      <div id="slider" class="nivoSlider">
        <?php
			   foreach($imgArr as $val){
				?>
        <img src="<?php echo $play['slide_image'.$val]; ?>" data-thumb="<?php echo $play['slide_image'.$val]; ?>" alt="" title="<?php echo esc_attr('#htmlcaption'.$val) ; ?>" />
        <?php } ?>
      </div>
      <?php foreach($imgArr as $val)	{ ?>
      <div id="htmlcaption<?php echo esc_attr($val); ?>" class="nivo-html-caption">
        <?php if(!empty($play['slide_title'.$val])){ ?>
        <div class="title"><?php echo $play['slide_title'.$val]; ?></div>
        <?php } ?>
        <?php if(!empty($play['slide_desc'.$val])){ ?>
        <div class="slidedesc"><?php echo $play['slide_desc'.$val]; ?></div>
        <?php } ?>
        <?php if(!empty($play['slide_btn'.$val])){ ?>
        <div class="slidebtn"><a class="slidelink" href="<?php echo $play['slide_link'.$val]; ?>"><?php echo $play['slide_btn'.$val]; ?></a></div>
        <?php } ?>        
      </div>
      <?php } ?>
    </div>
    <?php }
	else { ?>
    <div class="slider-wrapper slide-temp theme-default">
      <div id="slider" class="nivoSlider">
        <?php foreach($ImageUrl as $val) { ?>
        <img src="<?php echo $val; ?>" data-thumb="<?php echo $val; ?>" alt="" title="#htmlcaption" />
        <?php } ?>
      </div>
      <?php for($i=1; $i<=3; $i++)	{ ?>
      <div id="htmlcaption" class="nivo-html-caption">
        <div class="title"><?php echo 'LEARNING CAN BE FUN<br/><span style="color:#ea5330;">BEST PLACE FOR CHILDREN</span>'; ?></div>
        <div class="slidedesc"><?php echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu est vitae diam aliquam consectetur. In egestas odio et pellentesque ultricies. Etiam vitae sem scelerisque, volutpat arcu ac, iaculis erat. Integer auctor ipsum sed dictum rutrum. Nunc at ligula ultrices, aliquet velit eget, sodales tellus'; ?></div>
        <div class="slidebtn"><a href="#"><?php echo 'Read More';?></a></div>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
</section>
