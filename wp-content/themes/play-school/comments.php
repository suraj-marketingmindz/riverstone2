<?php 
/**
 * The Comment section for Playschool
 *
 * Displays the Comment section in posts and pages.
 *
 * @package play-school
 * 
 * @since play-school 1.0
 */
global $play;?>

<?php
	//If Password Protected, DO NOT LOAD
	if ( post_password_required() ) { ?>
	<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'play-school'); ?></p>
	<?php
	return;
	}
?>
 
<!-- THE COMMENTS/PING TEMPLATE START -->
<?php if ( have_comments() ) : ?>
<?php if ( ! empty($comments_by_type['comment']) ) : ?>

    <!--COMMENT RESPONSE COUNT START-->
        <h3 id="comments">
            <?php comments_number(__( 'No Responses', 'play-school'), __('One Response', 'play-school'), __('% Responses', 'play-school'));?> to &#8220;<a><?php the_title(); ?></a>&#8221;
        </h3>
    <!--COMMENT RESPONSE COUNT END-->
    
     
    <!--COMMENTS LIST START--> 
        <ul class="commentlist">	
            <!--Comments callback from functions.php-->
            <?php wp_list_comments('type=comment&callback=complete_comment');?>
        </ul>
    
        <!--Comments page navigation-->
        <div class="navigation">
        <?php paginate_comments_links( array('prev_text' => '&laquo;', 'next_text' => '&raquo;')) ?> 
        </div>
    <!--COMMENTS LIST END-->


<?php endif; ?>

<!--PINGS START-->
<?php if ( ! empty($comments_by_type['pings']) ) : ?>

	<!--PINGS Title-->
    <h3 id="comments_ping"><?php _e('Trackbacks &amp; Pings', 'play-school'); ?></h3>
     
    <!--PINGS LIST START--> 
        <ul class="commentlist" id="ping">
        <?php wp_list_comments('type=pings&callback=complete_ping'); ?>
        </ul>
        
        <div class="navigation"><?php paginate_comments_links( array('prev_text' => '&laquo;', 'next_text' => '&raquo;')) ?></div>
    <!--PINGS LIST END-->

<?php endif; ?>
<!--PINGS END-> 



<?php else : // this is displayed if there are no comments so far ?>
 
<?php if ('open' == $post->comment_status) : ?>
<!-- If comments are open, but there are no comments. -->
 
<?php else : // comments are closed ?>
<!-- If comments are closed. -->
<?php if ( !is_page() ) { ?><p class="nocomments"><?php _e('Comments are closed.', 'play-school'); ?></p><?php } ?>
 
<?php endif; ?>
<?php endif; ?>
 

<!--COMMENT FORM START--> 
	<?php  comment_form(); ?>
<!--COMMENT FORM END--> 