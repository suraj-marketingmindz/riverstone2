<?php 
/**
 * The index page of Play School
 *
 * Displays the home page elements.
 *
 * @package play-school
 * 
 * @since play-school 1.0
 */
global $play;
?>
<?php get_header(); ?>
<?php if ( is_front_page() ) { ?>

<div class="fixed_site layer_wrapper">
  <div class="fixed_wrap fixindex"> 
    <!-- Featture Boxes Section Start -->
    <div class="featured_area <?php if($play['homeblock_bg_setting']){ ?>featured_area_bg<?php } ?> <?php if(!empty($play['hide_boxes'])){ echo 'hide_section';} ?>" <?php if(!empty($play['ftd_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $ftdbgvideo = $play['ftd_bg_video']; echo do_shortcode($ftdbgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
      <div class="center">
        <div class="centertitle">
        <h2>
          <?php if (!empty ($play['featured_section_title'])) { ?>
          <?php $ftd = html_entity_decode($play['featured_section_title']); $ftd = stripslashes($ftd); echo do_shortcode($ftd); ?>
          <?php } ?>
        </h2>
        </div>
        <div class="sectionrow">
<?php
$pages = array();
for ( $count = 1; $count <= 4; $count++ ) {
	$mod = get_theme_mod( 'page-setting' . $count );
	if ( 'page-none-selected' != $mod ) {
		$pages[] = $mod;
	}
}
$filterArray = array_filter($pages);
if(count($filterArray) == 0){ ?>
<?php
for($bx=1; $bx<4; $bx++) { ?>
<div class="featured_block fblock3 <?php if($bx%3==0){ ?>no_margin_right<?php } ?>"> <a href="<?php echo esc_url('#');?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/featured_icon<?php echo esc_attr($bx); ?>.png"></a>
          <h3><a href="<?php echo esc_url('#');?>">
          	<?php if ($bx == 1) {?>
            <?php esc_attr_e('Target Skillsets','play-school'); ?>
            <?php } ?>
            <?php if ($bx == 2) {?>
            <?php esc_attr_e('Kids Play Ground','play-school'); ?>
            <?php } ?>
            <?php if ($bx == 3) {?>
            <?php esc_attr_e('Bus Facility','play-school'); ?>
            <?php } ?>    
            </a></h3>
          <p>
            <?php esc_attr_e('Quisque commodo condimentum sollicitudin. Vestibulum nec posuere purus. Proin venenatis scelerisque dui sit amet placerat. Phasellus sit amet hendrerit lectus, sit amet blandit ligula. Maecenas in elit a purus porta egestas.','play-school');?>
          </p>
          <a class="sktmore" href="<?php echo esc_url('#');?>" style="color:#FFF;">
          <?php esc_attr_e('Read More','play-school');?>
          </a>  </div>
	
<?php } ?>                    
<?php 	
}else{

$filled_array=array_filter($pages);
$classNo = count($filled_array);	
	
$args = array(
	'posts_per_page' => 4,
	'post_type' => 'page',
	'post__in' => $pages,
	'orderby' => 'post__in'
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) :
	$count = 1;
	while ( $query->have_posts() ) : $query->the_post();
	?>
	<div class="featured_block <?php echo 'fblock'.$classNo; if($count % $classNo == 0){ ?> no_margin_right<?php } ?>"> <a href="<?php the_permalink(); ?>">
         <?php if(!empty($play['page-setting'.$count.'_image']['url'])){   ?>
         <img src="<?php $pgimg = $play['page-setting'.$count.'_image']; echo $pgimg['url']; ?>" />
         <?php } ?>
          </a>
          <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <p><?php echo wp_trim_words( get_the_content(), $play['featured_excerpt'] ); ?></p>
          <a href="<?php the_permalink(); ?>" class="sktmore">
          <?php if (!empty ($play['featured_block_button'])) { ?>
          <?php $ftdbutton = html_entity_decode($play['featured_block_button']); $ftdbutton = stripslashes($ftdbutton); echo do_shortcode($ftdbutton); ?>
          <?php } ?>
          </a> </div>
        <?php if($count % $classNo == 0) { ?>
        <div class="clear"></div>
        <?php } ?>
	<?php
	$count++;
	endwhile;
 endif;
}
 
?></div>
<div class="clear"></div>
      </div>
    </div>
    <!-- Featture Boxes Section End --> 
    <!-- Home Section 1 -->
    <div class="home1_section_area <?php if($play['section1_bg_image']){ ?>home1_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section1'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section1_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec1bgvideo = $play['section1_bg_video']; echo do_shortcode($sec1bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section1_title'])) { ?>
          <?php $sct1 = html_entity_decode($play['section1_title']); $sct1 = stripslashes($sct1); echo do_shortcode($sct1); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section1_content">
             <?php $sec1cntnt = $play['section1_content']; echo do_shortcode($sec1cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 1 -->
    <!-- Home Section 2 -->
    <div class="home2_section_area <?php if($play['section2_bg_image']){ ?>home2_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section2'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section2_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec2bgvideo = $play['section2_bg_video']; echo do_shortcode($sec2bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section2_title'])) { ?>
          <?php $sct2 = html_entity_decode($play['section2_title']); $sct2 = stripslashes($sct2); echo do_shortcode($sct2); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section2_content">
             <?php $sec2cntnt = $play['section2_content']; echo do_shortcode($sec2cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 2 -->   
    <!-- Home Section 3 -->
    <div class="home3_section_area <?php if($play['section3_bg_image']){ ?>home3_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section3'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section3_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec3bgvideo = $play['section3_bg_video']; echo do_shortcode($sec3bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section3_title'])) { ?>
          <?php $sct3 = html_entity_decode($play['section3_title']); $sct3 = stripslashes($sct3); echo do_shortcode($sct3); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section3_content">
             <?php $sec3cntnt = $play['section3_content']; echo do_shortcode($sec3cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 3 -->    
    <!-- Home Section 4 -->
    <div class="home4_section_area <?php if($play['section4_bg_image']){ ?>home4_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section4'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section4_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec4bgvideo = $play['section4_bg_video']; echo do_shortcode($sec4bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section4_title'])) { ?>
          <?php $sct4 = html_entity_decode($play['section4_title']); $sct4 = stripslashes($sct4); echo do_shortcode($sct4); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section4_content">
             <?php $sec4cntnt = $play['section4_content']; echo do_shortcode($sec4cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 4 --> 
    <!-- Home Section 5 -->
    <div class="home5_section_area <?php if($play['section5_bg_image']){ ?>home5_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section5'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section5_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec5bgvideo = $play['section5_bg_video']; echo do_shortcode($sec5bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section5_title'])) { ?>
          <?php $sct5 = html_entity_decode($play['section5_title']); $sct5 = stripslashes($sct5); echo do_shortcode($sct5); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section5_content">
             <?php $sec5cntnt = $play['section5_content']; echo do_shortcode($sec5cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 5 --> 
    <!-- Home Section 6 -->
    <div class="home6_section_area <?php if($play['section6_bg_image']){ ?>home6_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section6'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section6_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec6bgvideo = $play['section6_bg_video']; echo do_shortcode($sec6bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section6_title'])) { ?>
          <?php $sct6 = html_entity_decode($play['section6_title']); $sct6 = stripslashes($sct6); echo do_shortcode($sct6); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section6_content">
             <?php $sec6cntnt = $play['section6_content']; echo do_shortcode($sec6cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 6 --> 
    <!-- Home Section 7 -->
    <div class="home7_section_area <?php if($play['section7_bg_image']){ ?>home7_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section7'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section7_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec7bgvideo = $play['section7_bg_video']; echo do_shortcode($sec7bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section7_title'])) { ?>
          <?php $sct7 = html_entity_decode($play['section7_title']); $sct7 = stripslashes($sct7); echo do_shortcode($sct7); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section7_content">
             <?php $sec7cntnt = $play['section7_content']; echo do_shortcode($sec7cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 7 --> 
    <!-- Home Section 8 -->
    <div class="home8_section_area <?php if($play['section8_bg_image']){ ?>home8_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section8'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section8_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec8bgvideo = $play['section8_bg_video']; echo do_shortcode($sec8bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section8_title'])) { ?>
          <?php $sct8 = html_entity_decode($play['section8_title']); $sct8 = stripslashes($sct8); echo do_shortcode($sct8); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section8_content">
             <?php $sec8cntnt = $play['section8_content']; echo do_shortcode($sec8cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 8 --> 
    <!-- Home Section 9 -->
    <div class="home9_section_area <?php if($play['section9_bg_image']){ ?>home9_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section9'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section9_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec9bgvideo = $play['section9_bg_video']; echo do_shortcode($sec9bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section9_title'])) { ?>
          <?php $sct9 = html_entity_decode($play['section9_title']); $sct9 = stripslashes($sct9); echo do_shortcode($sct9); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section9_content">
             <?php $sec9cntnt = $play['section9_content']; echo do_shortcode($sec9cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 9 --> 
    <!-- Home Section 10 -->
    <div class="home10_section_area <?php if($play['section10_bg_image']){ ?>home10_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section10'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section10_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec10bgvideo = $play['section10_bg_video']; echo do_shortcode($sec10bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        	<div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section10_title'])) { ?>
          <?php $sct10 = html_entity_decode($play['section10_title']); $sct10 = stripslashes($sct10); echo do_shortcode($sct10); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section10_content">
             <?php $sec10cntnt = $play['section10_content']; echo do_shortcode($sec10cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 10 --> 
    <!-- Home Section 11 -->
    <div class="home11_section_area <?php if($play['section11_bg_image']){ ?>home11_section_area_bg<?php } ?>  
<?php if(!empty($play['hide_boxes_section11'])){ echo 'hide_section';} ?>" <?php if(!empty($play['section11_bg_video'])){ ?>data-vidbg-bg="mp4: <?php $sec11bgvideo = $play['section11_bg_video']; echo do_shortcode($sec11bgvideo); ?>" data-vidbg-options="loop: true, muted: true, overlay: false"<?php } ?>>
    	<div class="center">
        <div class="centertitle">
        	<h2>
          <?php if (!empty ($play['section11_title'])) { ?>
          <?php $sct11 = html_entity_decode($play['section11_title']); $sct11 = stripslashes($sct11); echo do_shortcode($sct11); ?>
          <?php } ?>
            </h2>
            </div>
            <div class="home_section11_content">
             <?php $sec11cntnt = $play['section11_content']; echo do_shortcode($sec11cntnt); ?>
            </div>
        </div>
    </div>
    <!-- Home Section 11 -->                                 
  </div>
</div>
<?php }else{ ?>
<div class="fixed_site">
  <div class="fixed_wrap fixindex">
    <?php get_template_part('templates/post','layout4'); ?>
  </div>
</div>
<?php } //is_front_page ENDS ?>
<?php get_footer(); ?>